import React, { Component } from 'react'
import { Bar, Doughnut } from 'react-chartjs-2';
import {
  barChart2,
  lineChart2,
  conditionsChart,
  barChart,
  doughnutChart
} from '../../../data'

class Customers extends Component {
  state = {
    lineChart2,
    barChart2,
    conditionsChart,
    barChart
  }

  render() {
    return (
      <React.Fragment> 
        <div className="row">
          <div className="col-sm-12"><a name="customers"></a>
            <h2 id="customers">Customers</h2>
          </div>
        </div>
        <div className="card px-4">
        <div className="row">
            <div className="col-12 col-lg-5">
            <div className="card-body">
            {/* <div className="row">
              <div className="col-5">
                <h4 className="card-title mb-2">
                <select className="form-control" id="changeCateg">
                  <option>Categ 1</option>
                  <option>Categ 2</option>
                  <option>Categ 3</option>
                  <option>Categ 4</option>
                  <option>Categ 5</option>
                </select>
                </h4>
                <div className="small text-muted">November 2018</div>
              </div>
              <div className="col-12 mt-3">
                <div className="btn-group btn-group-toggle float-right mr-3" data-toggle="buttons">
                  <label className="btn btn-outline-secondary">
                    <input id="option1" type="radio" name="options" autoComplete="off"></input> New
                  </label>
                  <label className="btn btn-outline-secondary">
                    <input id="option2" type="radio" name="options" autoComplete="off" ></input> Old
                  </label>
                </div>
                
                
              </div>
            </div> */}
            <div className="chart-wrapper mt-4">
              <Bar data={barChart.data}
                    options={barChart.options}
                    legend={barChart.options.legend}
                    className ='chart'
                    height={200}
                    id="barChart" />
            </div>
          </div>
            </div>
            <div className="col-12 col-lg-7">
              <div className="mt-2 card-body">
              <div className="row mx-1">
              </div>
              <table className="mt-4 table table-dark table-responsive-sm table-bordered">
                <thead>
                  <tr>
                    <th>Customer Type</th>
                    <th>Return</th>
                    <th>Conver.</th>
                    <th>AOV</th>
                    <th>AIV</th>
                    <th>Another</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Pompeius René</td>
                    <td>2012/01/01</td>
                    <td>Member</td>
                    <td>57</td>
                    <td>57</td>
                    <td>57</td>
                  </tr>
                  <tr>
                    <td>Paĉjo Jadon</td>
                    <td>2012/02/01</td>
                    <td>Staff</td>
                    <td>57</td>
                    <td>57</td>
                    <td>57</td>
                  </tr>
                  <tr>
                    <td>Micheal Mercurius</td>
                    <td>2012/02/01</td>
                    <td>Admin</td>
                    <td>57</td>
                    <td>57</td>
                    <td>57</td>
                  </tr>
                  <tr>
                    <td>Ganesha Dubhghall</td>
                    <td>2012/03/01</td>
                    <td>Member</td>
                    <td>57</td>
                    <td>57</td>
                    <td>57</td>
                  </tr>
                  <tr>
                    <td>Hiroto Šimun</td>
                    <td>2012/01/21</td>
                    <td>Staff</td>
                    <td>57</td>
                    <td>57</td>
                    <td>57</td>
                  </tr>
                  <tr>
                    <td>Hiroto Šimun</td>
                    <td>2012/01/21</td>
                    <td>Staff</td>
                    <td>57</td>
                    <td>57</td>
                    <td>57</td>
                  </tr>
                </tbody>
                </table>
              </div>
            </div>
            <div className="col-12 col-lg-5">
            <div className="card-body">
              <div className="row mx-1">
              </div>
                <div className="chart-wrapper" style={{marginTop:"40px"}}>
                  <Doughnut data={doughnutChart.data}
                        options={doughnutChart.options}
                        legend={doughnutChart.options.legend}
                        className ='chart'
                        height={200}
                        id="doughnutChart" />
                </div>
              </div>
            </div>
            <div className="col-12 col-lg-7">
            <div className="card-body">
              
              
              <div className="chart-wrapper" style={{marginTop:"40px"}}>
                <Bar data={barChart2.data}
                      options={barChart2.options}
                      legend={barChart2.options.legend}
                      className ='chart'
                      height={150}
                      id="barChart" />
                </div>
              </div>
            </div>
          </div>
        </div>
    </React.Fragment>
    
      
    )
  }
}

export default Customers
