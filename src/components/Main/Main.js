import React, { Component } from 'react'

import Overview from './Overview/Overview';
// import Recommendations from './Recommendations/Recommendations';
import Customers from './Customers/Customers';
import SingleCustomer from './SingleCustomer/SingleCustomer';

class Main extends Component {
  
  constructor(props) {
    super(props);
    
    this.state = { SingleCustomerShown: false}

    this.handleChange = this.handleChange.bind(this)
  }
  
  componentDidMount(){
    
  }
  
  handleChange(e){
    
    if (e.target.value === "notShown"){
      this.setState({SingleCustomerShown: false})
    }
    else {
      this.setState({SingleCustomerShown: true})
    }
    
  }

  render() {
    return (
      <main className="main">
        <div className="container-fluid mt-3">
          <div className="animated fadeIn">
            <Overview></Overview>
            <Customers></Customers>
            <div className="row my-5">
              <div className="col-12 col-md-4"><a name="customers"></a>
                <h2 id="customers">Single Customer</h2>
              </div>
              <div className="col-12 col-md-3 float-left ml-0 pl-0">
                <select onChange={this.handleChange} className="form-control" id="changeCateg">
                  <option value="notShown" >Select Customer ID</option>
                  <option value="1" >1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                </select>
              </div>
            </div>
            {this.state.SingleCustomerShown?<SingleCustomer></SingleCustomer>:null}
            {/*
            {/* <Recommendations></Recommendations> */}
             {/* } */}
          </div>
        </div>
      </main>
    )
  }
}

export default Main
