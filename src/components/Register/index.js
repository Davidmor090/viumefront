import React, { Component } from 'react';
import api from '../../services';
import UserContext from '../../UserContext';
import SimpleReactValidator from 'simple-react-validator';

class Register extends Component {
  static contextType = UserContext;

  constructor(props) {
    super(props);

    this.validator = new SimpleReactValidator();

    this.initialData = '';

    localStorage.clear();

    this.state = {
      name: '', // name of the company
      address: '', // address of the company
      cif: '', // fiscal identification
      phone: '', // contact phone
      acceptConditions: false, // company has accepted conditions?
      country: 'ES',
      currency: 'EUR',
      ecommerce: '', // website where implement the <viume-recommender> tag
      website: '', // no-ecommerce site (optional)
      userName: '',
      userMail: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleFiles = this.handleFiles.bind(this);
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  handleFiles() {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.onload = event => {
      this.initialData = event.target.result;
    };

    reader.readAsText(file);
  }

  //var csv is the CSV file with headers
  csvJSON(csv) {
    const lines = csv.split('\n');

    const headers = lines[0].split(',');
    const obj = {};

    for (let i = 0; i < headers.length; i++) {
      obj[headers[i]] = [];
      console.log(headers[i]);
      for (var j = 1; j < lines.length; j++) {
        const line = lines[j].split(',');
        obj[headers[i]].push(line[i]);
      }
    }

    console.log(obj);
    return obj; //JSON
  }

  handleSubmit = async e => {
    e.preventDefault();

    if (this.validator.allValid()) {
      console.log('all is valid');
    } else {
      console.log('all is not valid');
    }

    await this.setState({
      initalData: this.csvJSON(this.initialData)
    });

    const res = await api.post(
      'http://206.189.166.235:3003/api2/new-business',
      this.state
    );

    if (!res) {
      this.setState(
        {
          error: true
        },
        () => {
          return false;
        }
      );
    }

    console.log('new business created');
    //this.props.history.push({ pathname: '/' });
    //const previousURL = window.location.href.split('/')[0];
    //window.location.href = `${previousURL}`;
  };

  showError() {
    if (this.state.error) {
      return (
        <div class='alert alert-danger'>Incorrect username or password</div>
      );
    }

    return '';
  }

  render() {
    return (
      <div className='app flex-row align-items-center'>
        <div className='container'>
          <div className='row justify-content-center'>
            <div className='col-md-6'>
              <img
                className='img-fluid'
                src='img/brand/viume-big.png'
                alt='Viume Logo'
              />
            </div>
          </div>
          <div className='row justify-content-center'>
            <div className='card-group col-12'>
              <div className='card p-4'>
                <div className='card-body'>
                  <form noValidate>
                    <div className='row'>
                      <div className='col-12'>
                        {this.showErrors}
                        <h1>General information</h1>
                        <p className='text-muted'>
                          Fill the company requested info below to join Viume
                          SaaS
                        </p>
                      </div>
                    </div>
                    <div className='row form-group'>
                      <div className='input-group col-6'>
                        <div className='input-group-prepend'>
                          <span className='input-group-text'>
                            <i className='fas fa-building' />
                          </span>
                        </div>
                        <input
                          className='form-control'
                          type='text'
                          name='name'
                          value={this.state.name}
                          onChange={this.handleChange}
                          placeholder='Company Name'
                          required='required'
                        />
                        {this.validator.message(
                          'Company Name is required',
                          this.state.name,
                          'required'
                        )}
                      </div>
                      <div className='input-group col-6'>
                        <div className='input-group-prepend'>
                          <span className='input-group-text'>
                            <i className='fas fa-map-marker-alt' />
                          </span>
                        </div>
                        <input
                          className='form-control'
                          type='text'
                          name='address'
                          value={this.state.address}
                          onChange={this.handleChange}
                          placeholder='Company Address'
                          required='required'
                        />
                      </div>
                    </div>
                    <div className='row form-group'>
                      <div className='input-group col-6'>
                        <div className='input-group-prepend'>
                          <span className='input-group-text'>
                            <i className='fas fa-fingerprint' />
                          </span>
                        </div>
                        <input
                          className='form-control'
                          type='text'
                          name='cif'
                          value={this.state.cif}
                          onChange={this.handleChange}
                          placeholder='Company CIF'
                          required='required'
                        />
                      </div>
                      <div className='input-group col-6'>
                        <div className='input-group-prepend'>
                          <span className='input-group-text'>
                            <i className='fas fa-phone' />
                          </span>
                        </div>
                        <input
                          className='form-control'
                          type='tel'
                          name='phone'
                          value={this.state.phone}
                          onChange={this.handleChange}
                          placeholder='Company Phone'
                          required='required'
                        />
                      </div>
                    </div>
                    <div className='row form-group'>
                      <div className='input-group col-6'>
                        <div className='input-group-prepend'>
                          <span className='input-group-text'>
                            <i className='fas fa-globe-europe' />
                          </span>
                        </div>
                        <input
                          className='form-control'
                          type='text'
                          name='country'
                          value={this.state.country}
                          onChange={this.handleChange}
                          placeholder='Company Country'
                          required='required'
                        />
                      </div>
                      <div className='input-group col-6'>
                        <div className='input-group-prepend'>
                          <span className='input-group-text'>
                            <i className='fas fa-coins' />
                          </span>
                        </div>
                        <select
                          className='form-control'
                          name='currency'
                          disabled='disabled'
                          value={this.state.currency}
                          required='required'
                        >
                          <option value='EUR'>EUR</option>
                        </select>
                      </div>
                    </div>
                    <div className='row form-group'>
                      <div className='input-group col-6'>
                        <div className='input-group-prepend'>
                          <span className='input-group-text'>
                            <i className='fas fa-globe' />
                          </span>
                        </div>
                        <input
                          className='form-control'
                          type='text'
                          name='website'
                          value={this.state.website}
                          onChange={this.handleChange}
                          placeholder='Company Website URL'
                          required='required'
                        />
                      </div>
                      <div className='input-group col-6'>
                        <div className='input-group-prepend'>
                          <span className='input-group-text'>
                            <i className='fas fa-store-alt' />
                          </span>
                        </div>
                        <input
                          className='form-control'
                          type='text'
                          name='ecommerce'
                          value={this.state.ecommerce}
                          onChange={this.handleChange}
                          placeholder='Company Ecommerce URL'
                          required='required'
                        />
                      </div>
                    </div>
                    <div className='row form-group'>
                      <div className='input-group col-6'>
                        <div className='input-group-prepend'>
                          <span className='input-group-text'>
                            <i className='fas fa-user' />
                          </span>
                        </div>
                        <input
                          className='form-control'
                          type='text'
                          name='userName'
                          value={this.state.userName}
                          onChange={this.handleChange}
                          placeholder='Main user name'
                          required='required'
                        />
                      </div>
                      <div className='input-group col-6'>
                        <div className='input-group-prepend'>
                          <span className='input-group-text'>
                            <i className='fas fa-at' />
                          </span>
                        </div>
                        <input
                          className='form-control'
                          type='email'
                          name='userMail'
                          value={this.state.userMail}
                          onChange={this.handleChange}
                          placeholder='Mail of the main user'
                          required='required'
                        />
                      </div>
                    </div>
                    <div className='row form-group'>
                      <div className='col-12'>
                        <h1>Upload your CSV </h1>
                        <p className='text-muted'>
                          We need to collect this information to train our
                          recommender with some data
                        </p>
                      </div>
                    </div>
                    <div className='row form-group'>
                      <div className='input-group col-6'>
                        <label htmlFor='file1'>
                          Upload a CSV with your information (delimited by
                          commas)
                        </label>
                        <input
                          id='file1'
                          className='form-control-file'
                          type='file'
                          name='initialData'
                          onChange={this.handleFiles}
                          placeholder='Upload the file in CSV format'
                          required='required'
                        />
                      </div>
                    </div>
                    <div className='row'>
                      <div className='col-12'>
                        <p className='text-muted'>
                          Pressing the Blue Button you accept the{' '}
                          <a href='#' target='_blank'>
                            terms & conditions
                          </a>
                          . Wait while the system is creating everything
                        </p>
                      </div>
                    </div>
                    <div className='row form-group'>
                      <div className='input-group col-12'>
                        <button
                          onClick={this.handleSubmit}
                          className='btn btn-primary btn-block'
                        >
                          Join Viume
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Register;
