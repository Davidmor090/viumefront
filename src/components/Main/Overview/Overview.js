import React, { Component } from 'react';
import { Line, Pie } from 'react-chartjs-2';
import api from '../../../services';
import {
  cardChart1,
  cardChart2,
  cardChart3,
  cardChart4,
  sparklineChart1,
  sparklineChart2,
  sparklineChart3,
  sparklineChart4,
  sparklineChart5,
  sparklineChart6,
  mainChart,
  mainChart2,
  mainChart3,
  mainChart4,
  pieChart
} from '../../../data';

class Overview extends Component {

  constructor(props) {
    super(props);
    
    this.state = { 
      loading: false,
      productos: [],
      revenueObject: [],
      AOV: 0,
      AIV: 0,
      startYear: 2019,
      startMonth: 1,
      startDay: 1,
      finalYear: 2019,
      finalMonth: 3,
      finalDay: 1,
      
      data: {
        labels : ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        datasets :[
          {
            label: 'Revenue',
            backgroundColor : 'rgba(151, 187, 205, 0.2)',
            borderColor : 'rgba(151, 187, 205, 1)',
            pointBackgroundColor : 'rgba(151, 187, 205, 1)',
            pointBorderColor : '#fff',
            data : []
          }]
        
      },
      cardChart1: cardChart1,
      cardChart2: cardChart2,
      cardChart3: cardChart3,
      cardChart4: cardChart4,
      sparkLine1: sparklineChart1,
      sparkLine2: sparklineChart2,
      sparkLine3: sparklineChart3,
      sparkLine4: sparklineChart4,
      sparkLine5: sparklineChart5,
      sparkLine6: sparklineChart6,
      mainChart: mainChart,
      mainChart2: mainChart2,
      mainChart3: mainChart3,
      mainChart4: mainChart4 
    };
    
    this.getRevenue = async () => {
      this.setState({loading: true})
      const body = {
        "startYear":parseInt(this.state.startYear),"startMonth":parseInt(this.state.startMonth),"startDay":parseInt(this.state.startDay),"endYear":parseInt(this.state.finalYear),"endMonth":parseInt(this.state.finalMonth),"endDay":parseInt(this.state.finalDay)
      }
      const res = await api.post("http://206.189.166.235:3003/api2/revenue/", body)
      // eslint-disable-next-line no-console
      console.log(res)
      this.setState(prevstate=>({
        data: {
          labels: Object.keys(res).map((values)=>{return values.split(" ")[0]}),
          datasets:[{
            ...prevstate.label,
            ...prevstate.backgroundColor,
            ...prevstate.borderColor,
            ...prevstate.pointBackgroundColor,
            ...prevstate.pointBorderColor,
            data: Object.values(res).map((values)=>{return Math.abs(Math.floor(values/5000))})
          }]
        }
      }))
      this.setState({loading: false})
    }

    this.getAOV = async () => {
      this.setState({loading: true})
      const body = 
        {
          "startYear":parseInt(this.state.startYear),"startMonth":parseInt(this.state.startMonth),"startDay":parseInt(this.state.startDay),"endYear":parseInt(this.state.finalYear),"endMonth":parseInt(this.state.finalMonth),"endDay":parseInt(this.state.finalDay)
        }
      const res = await api.post("http://206.189.166.235:3003/api2/aov", body)
      // eslint-disable-next-line no-console
      console.log(res)
      this.setState({AOV: Math.floor(res)}) 
      this.setState({loading: false}) 
    }

    this.getAIV = async () => {
      const body = 
        {
          "startYear":parseInt(this.state.startYear),"startMonth":parseInt(this.state.startMonth),"startDay":parseInt(this.state.startDay),"endYear":parseInt(this.state.finalYear),"endMonth":parseInt(this.state.finalMonth),"endDay":parseInt(this.state.finalDay)
        }
      const res = await api.post("http://206.189.166.235:3003/api2/aiv", body)
      // eslint-disable-next-line no-console
      console.log(res)
      this.setState({AIV: Math.floor(res)})
    }

    this.getTopProducts = async () => {
      const body = 
        {
          "startYear":parseInt(this.state.startYear),"startMonth":parseInt(this.state.startMonth),"startDay":parseInt(this.state.startDay),"endYear":parseInt(this.state.finalYear),"endMonth":parseInt(this.state.finalMonth),"endDay":parseInt(this.state.finalDay), "limit":5
        }
      const res = await api.post("http://206.189.166.235:3003/api2/top-products", body)
      // eslint-disable-next-line no-console
      console.log(res)
      this.setState({productos: res})
    }

    this.getRevenueData = function(){
      return Object.value(this.state.revenueObject)
    }

    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleChange = this.handleChange.bind(this)
  }

  handleSubmit(e){
    e.preventDefault()
    
    this.getAOV()
    this.getAIV()
    this.getTopProducts()
    this.getRevenue()
    
  }

  handleChange(e){
    this.setState({[e.target.name]: e.target.value })

  }

  componentDidMount(){
    
    this.getAOV()
    this.getAIV()
    this.getTopProducts()
    this.getRevenue()
    

    
  }

  render() {
    return (
      <React.Fragment>
        <div className="row">
          
          <div className="col-sm-12"><a name="overview"></a>
            <h2 id="overview">Overview</h2>
          </div>
          <div className="col-sm-12">
          <div className="my-4 border rounded px-2 py-2">
            <form className="form-inline" onSubmit={this.handleSubmit}>
            <div className="form-group">
            <label className=" mx-2 font-weight-bold text-muted">From:</label>
              <select onChange={this.handleChange} className="form-control" name="startYear">
                <option value="2019">2019</option>
                <option value="2018">2018</option>
                <option value="2017">2017</option>
                <option value="2016">2016</option>
                <option value="2015">2015</option>
                <option value="2014">2014</option>
              </select>
              <select onChange={this.handleChange} className="form-control" name="startMonth">
                <option value="1">Enero</option>
                <option value="2">Febrero</option>
                <option value="3">Marzo</option>
                <option value="4">Abril</option>
                <option value="5">Mayo</option>
                <option value="6">Junio</option>
                <option value="7">Julio</option>
                <option value="8">Agosto</option>
                <option value="9">Septiembre</option>
                <option value="10">Octubre</option>
                <option value="11">Noviembre</option>
                <option value="12">Diciembre</option>                
              </select>
              <select onChange={this.handleChange} className="form-control" name="startDay">
              <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
                <option value="13">13</option>
                <option value="14">14</option>
                <option value="15">15</option>
                <option value="16">16</option>
                <option value="17">17</option>
                <option value="18">18</option>
                <option value="19">19</option>
                <option value="20">20</option>
                <option value="21">21</option>
                <option value="22">22</option>
                <option value="23">23</option>
                <option value="24">24</option>
                <option value="25">25</option>
                <option value="26">26</option>
                <option value="27">27</option>
                <option value="28">28</option>
                <option value="29">29</option>
                <option value="30">30</option>
                <option value="31">31</option>
              </select>
              <label className=" mx-2 font-weight-bold text-muted">To:</label>
              <select onChange={this.handleChange} className="form-control" name="finalYear">
                <option value="2019">2019</option>
                <option value="2018">2018</option>
                <option value="2017">2017</option>
                <option value="2016">2016</option>
                <option value="2015">2015</option>
                <option value="2014">2014</option>
              </select>
              <select onChange={this.handleChange} className="form-control" name="finalMonth">
                <option value="1">Enero</option>
                <option value="2">Febrero</option>
                <option selected value="3">Marzo</option>
                <option value="4">Abril</option>
                <option value="5">Mayo</option>
                <option value="6">Junio</option>
                <option value="7">Julio</option>
                <option value="8">Agosto</option>
                <option value="9">Septiembre</option>
                <option value="10">Octubre</option>
                <option value="11">Noviembre</option>
                <option value="12">Diciembre</option>                  
              </select>
              <select onChange={this.handleChange} className="form-control"  name="finalDay">
              <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
                <option value="13">13</option>
                <option value="14">14</option>
                <option value="15">15</option>
                <option value="16">16</option>
                <option value="17">17</option>
                <option value="18">18</option>
                <option value="19">19</option>
                <option value="20">20</option>
                <option value="21">21</option>
                <option value="22">22</option>
                <option value="23">23</option>
                <option value="24">24</option>
                <option value="25">25</option>
                <option value="26">26</option>
                <option value="27">27</option>
                <option value="28">28</option>
                <option value="29">29</option>
                <option value="30">30</option>
                <option value="31">31</option>
              </select>
              <br></br>
              <button className="ml-2 btn btn-primary" type="submit">Enviar</button>
              </div>
            </form>
          </div>
          </div>
        </div>
        <div className="card px-4">
        <div className="row">
          <div className="col-sm-6 col-lg-3">
            <div className="mt-4 card text-white bg-darkContrast">
              <div className="card-body pb-0">
                <div className="btn-group float-right">
                  <button className="btn btn-transparent dropdown-toggle p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i className="icon-settings"></i></button>
                  <div className="dropdown-menu dropdown-menu-right"><a className="dropdown-item" href="#">Action</a><a className="dropdown-item" href="#">Another action</a><a className="dropdown-item" href="#">Something else here</a></div>
                </div>
                <div className="text-value">{this.state.loading ? <i className="fa fa-refresh fa-spin"></i> : this.state.AOV+'€'}</div>
                <div>AOV</div>
              </div>
              <div className="chart-wrapper mx-3">
                <Line data={cardChart1.data}
                      options={cardChart1.options}
                      legend={cardChart1.options.legend}
                      className ='chart'
                      height={80}
                      id="card-chart1" />
              </div>
            </div>
          </div>
          <div>{this.state.revenueObject?Object.values(this.state.revenueObject).map((value,index)=>{<p key={index}>{value}</p>}):null}</div>
          <div className="col-sm-6 col-lg-3">
            <div className="mt-4 card text-white bg-darkContrast">
              <div className="card-body pb-0">
                <button className="btn btn-transparent p-0 float-right" type="button"><i className="icon-location-pin"></i></button>
                <div className="text-value">{this.state.loading ? <i className="fa fa-refresh fa-spin"></i> : this.state.AIV+'€'}</div>
                <div>AIV</div>
              </div>
              <div className="chart-wrapper mx-3">
                <Line data={cardChart2.data}
                      options={cardChart2.options}
                      legend={cardChart2.options.legend}
                      className ='chart'
                      height={80}
                      id="card-chart2" />
              </div>
            </div>
          </div>
          
          <div className="col-sm-6 col-lg-3">
            <div className="card mt-4 text-white bg-darkContrast">
              <div className="card-body pb-0">
                <div className="btn-group float-right">
                  <button className="btn btn-transparent dropdown-toggle p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i className="icon-settings"></i></button>
                  <div className="dropdown-menu dropdown-menu-right"><a className="dropdown-item" href="#">Action</a><a className="dropdown-item" href="#">Another action</a><a className="dropdown-item" href="#">Something else here</a></div>
                </div>
                <div className="text-value">121 vs 57</div>
                <div>Completed survey vs Drop out</div>
              </div>
              <div className="chart-wrapper mx-3">
                <Line data={cardChart3.data}
                      options={cardChart3.options}
                      legend={cardChart3.options.legend}
                      className ='chart'
                      height={80}
                      id="card-chart3" />
              </div>
            </div>
          </div>
          <div className="col-sm-6 col-lg-3">
            <div className="card mt-4 text-white bg-darkContrast">
              <div className="card-body pb-0">
                <div className="btn-group float-right">
                  <button className="btn btn-transparent dropdown-toggle p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i className="icon-settings"></i></button>
                  <div className="dropdown-menu dropdown-menu-right"><a className="dropdown-item" href="#">Action</a><a className="dropdown-item" href="#">Another action</a><a className="dropdown-item" href="#">Something else here</a></div>
                </div>
                <div className="text-value">320 vs 120</div>
                <div>New customers vs Recurrent</div>
              </div>
              <div className="chart-wrapper mx-3">
                <Line data={cardChart4.data}
                      options={cardChart4.options}
                      legend={cardChart4.options.legend}
                      className ='chart'
                      height={80}
                      id="card-chart4" />
              </div>
            </div>
          </div>
        </div>
        <hr></hr>
          <div className="row mt-4">
            <div className="col-12">
              <h2>TOP SOLD PRODUCTS</h2>
              <table className="mt-4 table table-dark table-responsive-sm table-bordered">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>COLOR</th>
                    <th>FAMILIA</th>
                    <th>SUBFAMILIA</th>
                    <th>ESTAMPADO</th>
                    <th>MATERIAL</th>
                    <th>CANTIDAD</th>
                  </tr>
                </thead>
                <tbody>
                  
                  { this.state.loading ? null: this.state.productos.map((value, index) => {
                    return <tr key={index}>
                      <td>{value._id}</td>
                      <td>{value.COLOR}</td>
                      <td>{value.FAMILIA}</td>
                      <td>{value.SUBFAMILIA}</td>
                      <td>{value["TIPO_ESTAMPADO"]}</td>
                      <td>{value["TIPO_MATERIAL"]}</td>
                      <td>{value.count}</td>
                    </tr>
                  }) }
                </tbody>
                </table>
                {this.state.loading ? 
                <div className="row">
                  <div className="text-center col-12">
                    <i className="font-size-2 fa fa-refresh fa-spin"></i>
                  </div>
                </div> : null}
            </div>
          </div>
          <hr></hr>
        <div className="row mt-4">
        <div className="col-12 col-lg-6">
          <div className="card-body">
            <div className="row">
              <div className="col-12 col-sm-4">
                <h4 className="card-title mb-0">Revenue</h4>
                <div className="small text-muted">November 2018</div>
              </div>
                {/* <div className="btn-group btn-group-toggle float-right mr-3" data-toggle="buttons">
                  <label className="btn btn-outline-secondary">
                    <input id="option1" type="radio" name="options" autoComplete="off"></input> Week
                  </label>
                  <label className="btn btn-outline-secondary active">
                    <input id="option2" type="radio" name="options" autoComplete="off" ></input> Month
                  </label>
                  <label className="btn btn-outline-secondary">
                    <input id="option3" type="radio" name="options" autoComplete="off"></input> Year
                  </label>
                </div> */}
                
            </div>
            <div className="chart-wrapper" style={{height:"300px", marginTop:"40px"}}>
              <Line data={this.state.data}
                    options={mainChart.options}
                    legend={mainChart.options.legend}
                    className ='chart'
                    height={300}
                    id="main-chart" />
            </div>
          </div>
        </div>
          <div className="col-12 col-lg-6">
            <div className="card-body">
                <div className="row">
                  <div className="col-sm-4 col-12">
                    <h4 className="card-title mb-0">Products</h4>
                    <div className="small text-muted">November 2018</div>
                  </div>
                    {/* <div className="btn-group btn-group-toggle float-right mr-3" data-toggle="buttons">
                      <label className="btn btn-outline-secondary">
                        <input id="option1" type="radio" name="options" autoComplete="off"></input> Week
                      </label>
                      <label className="btn btn-outline-secondary active">
                        <input id="option2" type="radio" name="options" autoComplete="off" ></input> Month
                      </label>
                      <label className="btn btn-outline-secondary">
                        <input id="option3" type="radio" name="options" autoComplete="off"></input> Year
                      </label>
                    </div> */}
                    
                </div>
                <div className="chart-wrapper" style={{marginTop:"40px"}}>
                  <Pie data={pieChart.data}
                        options={pieChart.options}
                        legend={pieChart.options.legend}
                        className ='chart'
                        height={200}
                        id="pieChart" />
                </div>
              </div>
            </div>
          </div>
        </div> 
      </React.Fragment>
    )
  }
}



export default Overview 
