import React, { Component } from 'react'
import Aside from '../Aside/Aside';
import Footer from '../Footer/Footer';
import Header from '../Header/Header';
import Sidebar from '../Sidebar/Sidebar';

class Profile extends Component {
  render() {
    return (
      <div className="app header-fixed sidebar-expanded aside-menu-fixed sidebar-lg-show">
        <Header></Header>
          <div className="app-body">
            <Sidebar></Sidebar>
            <div className="container">
              <div className="row">
                <div className="col-8 mx-auto text-center my-2">
                  <h1 className="my-4">My Profile</h1>
                </div>
              </div>
            </div>
            <Aside></Aside>
          </div>
        <Footer></Footer>
      </div>  
    )
  }
}

export default Profile
