var COOKIE_DAYS = 7;
var DAY_SECONDS = 86400000;

var setCookie = function setCookie(name, value, days, path) {
  if (days === void 0) {
    days = COOKIE_DAYS;
  }

  if (path === void 0) {
    path = '/';
  }

  var expires = new Date(Date.now() + days * DAY_SECONDS).toUTCString();
  value = encodeURIComponent(value);
  document.cookie = name + "=" + value + "; expires=" + expires + "; path=" + path;
};

var getCookie = function getCookie(name) {
  return document.cookie.split('; ').reduce(function (r, v) {
    var _v$split = v.split('='),
        n = _v$split[0],
        val = _v$split.slice(1);

    return n === name ? decodeURIComponent(val.join('=')) : r;
  }, '');
};

var deleteCookie = function deleteCookie(name, path) {
  return setCookie(name, '', -1, path);
};
//# sourceMappingURL=cookies.js.map