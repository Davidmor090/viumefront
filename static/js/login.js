var names = ['silvia', 'david', 'jordi', 'victor'];

var login = function login(j) {
  j('.btn-login').on('click', function () {
    var username = j('input[type=text]').val();
    var password = j('input[type=password]').val();

    if (names.includes(username) && password === 'viume') {
      setCookie('viume-login', true);
      location.href = '/index.html';
    } else {
      j('.alert').removeClass('d-none');
    }
  });
  j('input[type=text],input[type=password]').on('keyup', function () {
    if (!j('.alert').hasClass('d-none')) {
      j('.alert').addClass('d-none');
    }
  });
};

login($);
//# sourceMappingURL=login.js.map