import React, { Component } from 'react'
// import { Bar, Doughnut } from 'react-chartjs-2';
import {
// barChart2,
// lineChart2,
// conditionsChart,
// barChart,
// doughnutChart
} from '../../../data'

class SingleCustomer extends Component {
  state = {}
  // lineChart2, barChart2, conditionsChart, barChart

  render() {
    return (
      <React.Fragment>
        <div className="row">
        <div className="col-sm-4 col-12">
        <div className="card">
          <div className="card-body">
            <div className="avatar float-left mr-3 pb-5 mb-3"><img className="img-avatar" src="img/avatars/4.jpg"></img></div>
            <h3 className="font-weight-bold">Olivia Young</h3>
            <div><i className="fa fa-envelope"></i>&nbsp;olivia@young.com</div>
            <div><i className="fa fa-location-arrow"></i>&nbsp;New York, NY</div>
          </div>
        </div>
      </div>
      <div className="col-sm-2 col-6">
        <div className="card">
          <div className="card-body p-3 d-flex align-items-center"><i className="fa fa-archive bg-warning p-3 font-2xl mr-3"></i>
            <div>
              <div className="text-value-sm text-warning">5</div>
              <div className="text-muted text-uppercase font-weight-bold small">Orders</div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-sm-2 col-6">
        <div className="card">
          <div className="card-body p-3 d-flex align-items-center"><i className="fa fa-money bg-primary p-3 font-2xl mr-3"></i>
            <div>
              <div className="text-value-sm text-primary">$415.52</div>
              <div className="text-muted text-uppercase font-weight-bold small">Total Spent</div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-sm-2 col-6">
        <div className="card">
          <div className="card-body p-3 d-flex align-items-center"><i className="fa fa-shopping-bag bg-success p-3 font-2xl mr-3"></i>
            <div>
              <div className="text-value-sm text-success">$83.10</div>
              <div className="text-muted text-uppercase font-weight-bold small">AOV</div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-sm-2 col-6">
        <div className="card">
          <div className="card-body p-3 d-flex align-items-center"><i className="fa fa-percent bg-danger p-3 font-2xl mr-3"></i>
            <div>
              <div className="text-value-sm text-danger">2 / Year</div>
              <div className="text-muted text-uppercase font-weight-bold small">Frequency</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className="row">
      <div className="col-sm-6 col-12">
        <div className="card">
          <div className="card-body">
            <h3>Customer ID: 123LMHY&7</h3>
            <div><a className="weatherwidget-io" href="https://forecast7.com/en/40d71n74d01/new-york/" data-label_1="NEW YORK" data-label_2="WEATHER" data-days="3" data-theme="original">NEW YORK WEATHER</a></div>
          </div>
        </div>
        <div className="card">
          <div className="card-body">
            <h3 className="mx-auto">Monthly Box Needed</h3><img className="img-fluid" src="img/icons/adasda.jpg"></img>
          </div>
        </div>
      </div>
      <div className="col-sm-6 col-12">
        <div className="card">
          <div className="card-body">
            <h3>60% Prepared for Winter</h3>
            <div className="row">
              <div className="col-sm-4 col-6 p-5 mx-auto"><img className="img-fluid" src="img/icons/Recurso 1155xxxhdpi.png" width="75%"></img></div>
              <div className="col-sm-4 col-6 p-5"><img className="img-fluid" src="img/icons/Recurso 1147xxxhdpi.png" width="75%"></img></div>
              <div className="col-sm-4 col-6 p-5"><img className="img-fluid" src="img/icons/Recurso 1152xxxhdpi.png" width="75%"></img></div>
            </div>
            <div className="row">
              <div className="col-sm-4 col-6"><img className="img-fluid" src="img/icons/Recurso 1153xxxhdpi.png" width="75%"></img></div>
              <div className="col-sm-4 col-6"><img className="img-fluid" src="img/icons/Recurso 1144xxxhdpi.png" width="75%"></img></div>
              <div className="col-sm-4 col-6"><img className="img-fluid" src="img/icons/Recurso 1154xxxhdpi.png" width="75%"></img></div>
            </div>
            <div className="row"><img className="img-fluid mx-auto" src="img/cuerpo.png"></img></div>
          </div>
        </div>
        <div className="card">
          <div className="card-body">
            <h5>Low Presence of Red in Wardrobe</h5>
            <div className="row">
              <div className="col-sm-6 col-12"><img className="img-fluid" src="img/piechart.png"></img></div>
              <div className="col-sm-6 col-12"><img className="img-fluid" src="img/ropa.png"></img></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </React.Fragment>)}

}

export default SingleCustomer;