import React, { Component } from 'react'
import { UserConsumer } from '../../UserContext';

import Aside from '../Aside/Aside';
import Footer from '../Footer/Footer';
import Header from '../Header/Header';
import Main from '../Main/Main';
import Sidebar from '../Sidebar/Sidebar';


class Home extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <React.Fragment>
        <div className="app header-fixed sidebar-expanded aside-menu-fixed sidebar-show">
        <Header></Header>
          <div className="app-body">
            <Sidebar></Sidebar>
            <Main></Main>
            <Aside></Aside>
          </div>
        <Footer></Footer>
        </div>
      </React.Fragment>
    )
  }
}

export default Home;
