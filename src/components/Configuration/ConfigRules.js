import React, { Component } from 'react'
import $ from 'jquery'
import api from '../../services'


class ConfigRules extends Component {
  
  constructor(props) {
    super(props);
    
    this.state = { 
      loading: false,
      alertOn: false,
      feedbackDisplay: "d-none",
      btnSendDisplay: "d-none",
      disableSend: false,

      UserID: 14,
      UserList: [],

      selectBodyshape: ["Rectangle", "Triangleup", "Trangledown", "Oval", "Hourglass", "Apple"],
      bodyshape: "",
      minage: 18,
      maxage: 65,
      age: "",
      selectSize: ["XS","S","M","L","XL","XXL"],
      size: "",
      selectGender: ["Man", "Woman"],
      gender: "",
      selectLocation: ['Alava','Albacete','Alicante','Almería','Asturias','Avila','Badajoz','Barcelona','Burgos','Cáceres',
        'Cádiz','Cantabria','Castellón','Ciudad Real','Córdoba','La Coruña','Cuenca','Gerona','Granada','Guadalajara',
        'Guipúzcoa','Huelva','Huesca','Islas Baleares','Jaén','León','Lérida','Lugo','Madrid','Málaga','Murcia','Navarra',
        'Orense','Palencia','Las Palmas','Pontevedra','La Rioja','Salamanca','Segovia','Sevilla','Soria','Tarragona',
        'Santa Cruz de Tenerife','Teruel','Toledo','Valencia','Valladolid','Vizcaya','Zamora','Zaragoza'],
      location: "",

      nboxes: "1",
      minproducts: "2",
      maxproducts: "5",
      selectPosition: ["Home Page", "Product Page", "My Profile"],
      position: "",
      btnFreq: false,
      CategoryFrequency: [],
      addoutfit: null,

      startYear: 2019,
      startMonth: 1,
      startDay: 1,
      finalYear: 2019,
      finalMonth: 3,
      finalDay: 1,
      
      selectSeason: [],
      season: "",
      minprice: "0",
      maxprice: "250",
      selectCategory: [],
      category: "",
      selectSubcategory: [],
      subcategory: "",
      selectOccasion: [],
      occasion: "",
      selectColor: [],
      color: "",
      selectItem: [],
      item: "",
      selectBrands: [],
      brands: "",
      
      boxResult : [],
      boxesResult: [],
      boxesResult2: [],
      noResults: false,
      noFeedbackResults: false,
      triggerFeedback: false,
      feedback: null,
      feedbackComment: "",
      allFeedback: [],
      disableSendFeedback: false,

      savedCustomerRules: [],
      savedOutfitRules: [],
      savedGeneralRules: [],
      ruleToRemove: ""
    };
    
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmitCustomer = this.handleSubmitCustomer.bind(this)
    this.handleSubmitOutfit = this.handleSubmitOutfit.bind(this)
    this.handleSubmitGeneral = this.handleSubmitGeneral.bind(this)
    this.handleClick = this.handleSubmitCustomer.bind(this)
    this.handleSend = this.handleSend.bind(this)
    this.handleSendFeedback = this.handleSendFeedback.bind(this)
    this.selectGeneralRules = this.selectGeneralRules.bind(this)
    this.selectOutfitRules = this.selectOutfitRules.bind(this)
    this.selectCustomerRules = this.selectCustomerRules.bind(this)
    this.removeCustomerRule = this.removeCustomerRule.bind(this)
    this.removeGeneralRule = this.removeGeneralRule.bind(this)
    this.removeOutfitRule = this.removeOutfitRule.bind(this)
    this.removeAllCustomerRules = this.removeAllCustomerRules.bind(this)
    this.removeAllOutfitRules = this.removeAllOutfitRules.bind(this)
    this.removeAllGeneralRules = this.removeAllGeneralRules.bind(this)
    this.selectUserID = this.selectUserID.bind(this)

  

    // const selectors = {
    //   freqString: JSON.stringify(this.state.categoryFrequency) || '',
    //   UserID: 14,
    //   nBoxes: this.state.nboxes || '',
    //   minProducts: this.state.minproducts || 0,
    //   maxProducts: this.state.maxproducts || 0,
    //   season: this.state.season || 'all',
    //   minCost: this.state.minprice || 0,
    //   maxCost: this.state.maxprice || 0,
    //   domain: this.state.domain || 'http://206.189.166.235:3003',
    //   path: this.state.path || '/api2/get_specRC/'
    // };


    this.getData = async () => {
      this.setState({feedbackDisplay: "d-none"})
      this.setState({noResults: false})
      this.setState({loading: true})
      const body = 
        {
          "UserID":this.state.UserID,"nBoxes":this.state.nboxes,"minProducts":this.state.minproducts,"maxProducts":this.state.maxproducts,"season":"all","minCost":this.state.minprice,"maxCost":this.state.maxprice,"freqString":JSON.stringify(this.state.CategoryFrequency)
        }
      const res = await api.post("http://206.189.166.235:3003/api2/get_specRC/", body)
      console.log(res)
      this.setState({boxesResult:res})
      if (this.state.boxesResult.length === 0) {
        this.setState({noResults: true})
      }
      this.setState({loading: false})
      this.setState({feedbackDisplay: "d-block"})
    }

    this.getDataForFeedback = async () => {
      this.setState({feedbackDisplay: "d-none"})
      this.setState({noFeedbackResults: false})
      this.setState({loading: true})
      const body = 
        {
          "UserID":this.state.UserID,"nBoxes":"1","minProducts":this.state.minproducts,"maxProducts":this.state.maxproducts,"season":"all","minCost":this.state.minprice,"maxCost":this.state.maxprice,"freqString":JSON.stringify(this.state.CategoryFrequency)
        }
      const res = await api.post("http://206.189.166.235:3003/api2/get_specRC/", body)
      console.log(res)
      this.setState({boxResult:res})
      if (this.state.boxResult.length === 0) {
        this.setState({noFeedbackResults: true})
      }
      this.setState({loading: false})
      this.setState({feedbackDisplay: "d-block"})
    }

    this.getFeedback = async () => {
      const res = await api.get("http://206.189.166.235:3003/api2/get_feedback/")
      console.log(res)
      this.setState({allFeedback: res})
    }

    this.saveFeedback = async () => {
      this.setState({loading: true})
      const body = {
        "FEEDBACK_ID": this.state.allFeedback.length+1, "STYLE_ID": this.getStyleID() || null, "USER_ID": this.state.UserID, "FEEDBACK": this.state.feedback, "DATE": this.getDate(), "COMMENT": this.state.feedbackComment, "BOX_ID": this.getBoxID() || null
      }
      const res = await api.post("http://206.189.166.235:3003/api2/save_feedback/", body)
      console.log(res)
      this.setState({loading: false})
      
    }

    this.getCategoriesData = async () => {
      this.setState({loading: true})
      const res = await api.get("http://206.189.166.235:3003/api2/get_product_categories/")
      console.log(res)
      this.setState({selectCategory: res})
      this.setState({loading: false})
      
    }

    this.getSubCategoriesData = async () => {
      this.setState({loading: true})
      const res = await api.get("http://206.189.166.235:3003/api2/get_product_subcategories/")
      console.log(res)
      this.setState({selectSubcategory: res})
      this.setState({loading: false})
      
    }

    this.getSeasonsData = async () => {
      this.setState({loading: true})
      const res = await api.get("http://206.189.166.235:3003/seasons")
      console.log(res)
      this.setState({selectSeasons: res})
      this.setState({loading: false})
      
    }

    this.getCustomerList = async () => {
      this.setState({loading: true})
      console.log("GENDER:", this.state.gender)
      const body = {
        "BODY_SHAPE":this.state.bodyshape, "GENDER": this.state.gender, "SIZE": this.state.size, "PROV_CLIENTE": this.state.location.toUpperCase(), "FECHA_NACIMIENTO_MIN": this.getMinBirthDate(this.state.maxage), "FECHA_NACIMIENTO_MAX": this.getMaxBirthDate(this.state.minage)
      }
      const res = await api.post("http://206.189.166.235:3003/api2/get_customers/", body)
      console.log(res)
      this.setState({UserList: res.map((value)=>{return value.userId})});
      this.setState({loading: false})
      res !== null ? this.setState({btnSendDisplay: "d-block"}): this.setState({btnSendDisplay: "d-none"});
      
    }

    this.getCustomerRules = async () => {
      const res = await api.get("http://206.189.166.235:3003/api2/get_cust_rules/")
      console.log(res)
      this.setState({savedCustomerRules: res})
    }

    // this.deleteCustomerRules = async (id) => {
    //   const body = JSON.stringify({"rule_id": id})
    //   // eslint-disable-next-line no-console
    //   console.log(body)
    //   const res = await api.remove("http://206.189.166.235:3003/api2/delete_cust_rules/", body)
    //   // eslint-disable-next-line no-console
    //   console.log(res)
    // }

    this.deleteCustomerRules = function(id) {
      
      const USER_TOKEN = localStorage.getItem("accessToken")
      const AuthStr = 'Bearer ' + USER_TOKEN;
      
      fetch("http://206.189.166.235:3003/api2/delete_cust_rules/",
        {
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': AuthStr
          },
          method: "DELETE",
          body: JSON.stringify({"rule_id": id})
          
        })
        .then(function(res){ return res.json() })
        .catch(function(res){ res })
      
    }

    this.deleteAllCustomerRules = function(){
      
      const USER_TOKEN = localStorage.getItem("accessToken")
      const AuthStr = 'Bearer ' + USER_TOKEN;

      fetch("http://206.189.166.235:3003/api2/delete_all_cust_rules/",
        {
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': AuthStr
          },
          method: "DELETE"
          
        })
        .then(function(res){ return res.json() })
        .catch(function(res){ res })
      
    }

    this.getOutfitRules = async () => {
      const res = await api.get("http://206.189.166.235:3003/api2/get_outfit_rules/")
      console.log(res)
      this.setState({savedOutfitRules: res})
    }

    this.deleteOutfitRules = function(id){
      
      const USER_TOKEN = localStorage.getItem("accessToken")
      const AuthStr = 'Bearer ' + USER_TOKEN;

      fetch("http://206.189.166.235:3003/api2/delete_outfit_rules/",
        {
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': AuthStr
          },
          method: "DELETE",
          body: JSON.stringify({"rule_id": id})
          
        })
        .then(function(res){ return res.json() })
        .catch(function(res){ res })
    }

    this.deleteAllOutfitRules = function(){
      
      const USER_TOKEN = localStorage.getItem("accessToken")
      const AuthStr = 'Bearer ' + USER_TOKEN;

      fetch("http://206.189.166.235:3003/api2/delete_all_outfit_rules/",
        {
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': AuthStr
          },
          method: "DELETE"
          
        })
        .then(function(res){ return res.json() })
        .catch(function(res){ res })
      
    }

    this.getGeneralRules = async () => {
      const res = await api.get("http://206.189.166.235:3003/api2/get_general_rules/")
      console.log(res)
      this.setState({savedGeneralRules: res})
    }

    this.deleteGeneralRules = function(id){
      
      const USER_TOKEN = localStorage.getItem("accessToken")
      const AuthStr = 'Bearer ' + USER_TOKEN;

      fetch("http://206.189.166.235:3003/api2/delete_general_rules/",
        {
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': AuthStr
          },
          method: "DELETE",
          body: JSON.stringify({"rule_id": id})
          
        })
        .then(function(res){ return res.json() })
        .catch(function(res){ res })
    }

    this.deleteAllGeneralRules = function(){
      
      const USER_TOKEN = localStorage.getItem("accessToken")
      const AuthStr = 'Bearer ' + USER_TOKEN;

      fetch("http://206.189.166.235:3003/api2/delete_all_general_rules/",
        {
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': AuthStr
          },
          method: "DELETE"
          
        })
        .then(function(res){ return res.json() })
        .catch(function(res){ res })
      
    }

    this.saveCustomerRules = async () => {
      this.setState({loading: true})
      const body = {
        "RULE_ID": this.state.savedCustomerRules.length+1, "BODY_SHAPE":this.state.bodyshape, "GENDER":this.state.gender, "SIZE": this.state.size, "PROV_CLIENTE": this.state.location.toUpperCase(), "FECHA_NACIMIENTO_MIN": this.getMinBirthDate(this.state.maxage), "FECHA_NACIMIENTO_MAX": this.getMaxBirthDate(this.state.minage)
      }
      const res = await api.post("http://206.189.166.235:3003/api2/save_cust_rules/", body)
      console.log(res)
      this.setState({loading: false})
    } 

    this.saveOutfitRules = async () => {
      this.setState({loading: true})
      const body = {
        "RULE_ID": this.state.savedOutfitRules.length+1 ,"SEASON": this.state.season, "MIN_PRICE":this.state.minprice, "MAX_PRICE": this.state.maxprice
      }
      const res = await api.post("http://206.189.166.235:3003/api2/save_outfit_rules/", body)
      console.log(res)
      this.setState({loading: false})
    } 
    
    this.saveGeneralRules = async () => {
      this.setState({loading: true})
      const body = {
        "RULE_ID": this.state.savedGeneralRules.length+1 ,"BOXES": this.state.nboxes, "MIN_PROD":this.state.minproducts, "MAX_PROD": this.state.maxproducts, "POSITION": this.state.position, "FREQUENCY": JSON.stringify(this.state.CategoryFrequency), "ADD_CART": this.state.addoutfit
      }
      const res = await api.post("http://206.189.166.235:3003/api2/save_general_rules/", body)
      console.log(res)
      this.setState({loading: false})
    } 

    this.saveBox = async () => {
      this.setState({loading: true})
      
      const PreProducts = this.state.boxResult.filter( (product) => { 
          return product.length > 1
      })
      const Products = PreProducts.map((product) => {
        return { "PRODUCT_ID": product[1] }
      })

      console.log(`BOX_ID: ${this.getBoxID()}, USER_ID: ${this.state.UserID}, PRODUCTS: ${Products}, STYLE_ID: ${this.getStyleID()}`)
      const body = {
        "BOX_ID": this.getBoxID(), "USER_ID":this.state.UserID, "PRODUCTS": Products  , "STYLE_ID": this.getStyleID() 
      }
      const res = await api.post("http://206.189.166.235:3003/api2/save_box/", body)
      
      console.log(res)
      this.setState({loading: false})
    } 

    this.getResultBox = function(boxObject){
      
      const productKeys = Object.values(boxObject).map((value)=>{return Object.keys(value)})[0]
      const productValues = (Object.values(boxObject).map((value)=>{return Object.values(value)})).join(", ")
      
      return "hello " + JSON.stringify(productKeys + " - " + productValues);
    }

    this.frequencyDisplay = function(freqString){
      let result = freqString.replace(/\[/g, '').replace(/\]/g, '');
      
      result = result.replace(/\{/g, '').replace(/\}/g, '');

      result = result.replace(/"/g, '')

      result = result.replace(/,/g, " ")

      result = result.replace(/:/g, ": ")

      return result;
    }

    this.getDate = function(){

      const date = new Date();

      return date;
    }

    this.getBoxID = function(){
      const box = this.state.boxResult[0]
      const BoxID = box[box.length-2]
      return BoxID
    }
    

    this.getStyleID = function(){
      const box = this.state.boxResult[0]
      const StyleID = box[box.length-1]
      return StyleID
    }
    
    
    this.getMinBirthDate = function(maxage){
      
      let actualYear = new Date().getFullYear();
      let dateMin = new Date()
      
      let yearOfBirthMin = actualYear - maxage
      
      dateMin.setFullYear(yearOfBirthMin)
      dateMin.setMonth(0)
      dateMin.setDate(1)
      dateMin.setHours(0)
      dateMin.setMinutes(0)
      dateMin.setSeconds(0)
      dateMin.setMilliseconds(0)
      
      let isoDateMin = dateMin.toISOString()
      
      
      return isoDateMin ;
    }

    this.getMaxBirthDate = function(minage){
      
      let actualYear = new Date().getFullYear();
      
      let dateMax = new Date()
      
      let yearOfBirthMax = actualYear - minage
      
      dateMax.setFullYear(yearOfBirthMax)
      dateMax.setMonth(0)
      dateMax.setDate(1)
      dateMax.setHours(0)
      dateMax.setMinutes(0)
      dateMax.setSeconds(0)
      dateMax.setMilliseconds(0)
      
      let isoDateMax = dateMax.toISOString()
      
      return isoDateMax ;
    }
    
    this.getAge = function(maxbirthdate){

      let actualYear = new Date().getFullYear();
      let yearofBirth = parseInt(maxbirthdate.split("-",1))
      let minAge = actualYear - yearofBirth

      return minAge ;
    }

    this.addCategoryFrequency = function(key, value){
    
      const obj = {[key]: value}
      const list = []
      list[0] = obj
      
      this.setState((prevState)=>
        ({
          CategoryFrequency: prevState.CategoryFrequency.concat(list)
        })
      )
      // this.state.CategoryFrequency.push(obj)
      // this.forceUpdate(()=>this.state.CategoryFrequency)
    }

    this.removeCategoryFrequency = function(item){
      console.log(this.state.CategoryFrequency)
      console.log(this.state.CategoryFrequency[item],item)
      
      const list = this.state.CategoryFrequency.filter((el) => el !== item)
      
      this.setState((prevState)=>({
        CategoryFrequency: list
      }))
      // this.state.CategoryFrequency.splice(index,index)
      // this.forceUpdate(()=>this.state.CategoryFrequency)
    }

    this.showGeneralRules = function(){
      
      if($("#showGeneralRules").hasClass("cui-chevron-top")){
        $("#showGeneralRules").removeClass("cui-chevron-top")
        $("#showGeneralRules").addClass("cui-chevron-bottom")
      }
      else if($("#showGeneralRules").hasClass("cui-chevron-bottom")){
        $("#showGeneralRules").removeClass("cui-chevron-bottom")
        $("#showGeneralRules").addClass("cui-chevron-top")
      }

      if($(".generalrulesdisplay").hasClass("d-none")){
        $(".generalrulesdisplay").removeClass("d-none")
        $(".generalrulesdisplay").addClass("d-block")
      }
      else if($(".generalrulesdisplay").hasClass("d-block")){
        $(".generalrulesdisplay").removeClass("d-block")
        $(".generalrulesdisplay").addClass("d-none")
      }
        
    }

    this.showOutfitRules = function(){
      
      if($("#showOutfitRules").hasClass("cui-chevron-top")){
        $("#showOutfitRules").removeClass("cui-chevron-top")
        $("#showOutfitRules").addClass("cui-chevron-bottom")
      }
      else if($("#showOutfitRules").hasClass("cui-chevron-bottom")){
        $("#showOutfitRules").removeClass("cui-chevron-bottom")
        $("#showOutfitRules").addClass("cui-chevron-top")
      }

      if($(".outfitrulesdisplay").hasClass("d-none")){
        $(".outfitrulesdisplay").removeClass("d-none")
        $(".outfitrulesdisplay").addClass("d-block")
      }
      else if($(".outfitrulesdisplay").hasClass("d-block")){
        $(".outfitrulesdisplay").removeClass("d-block")
        $(".outfitrulesdisplay").addClass("d-none")
      }
        
    }

    this.showCustomerRules = function(){
      
      if($("#showCustomerRules").hasClass("cui-chevron-top")){
        $("#showCustomerRules").removeClass("cui-chevron-top")
        $("#showCustomerRules").addClass("cui-chevron-bottom")
      }
      else if($("#showCustomerRules").hasClass("cui-chevron-bottom")){
        $("#showCustomerRules").removeClass("cui-chevron-bottom")
        $("#showCustomerRules").addClass("cui-chevron-top")
      }

      if($(".customerrulesdisplay").hasClass("d-none")){
        $(".customerrulesdisplay").removeClass("d-none")
        $(".customerrulesdisplay").addClass("d-block")
      }
      else if($(".customerrulesdisplay").hasClass("d-block")){
        $(".customerrulesdisplay").removeClass("d-block")
        $(".customerrulesdisplay").addClass("d-none")
      }
        
    }

    this.showSavedRules = () => {

      if($("#showSavedRules").hasClass("cui-chevron-top")){
        $("#showSavedRules").removeClass("cui-chevron-top")
        $("#showSavedRules").addClass("cui-chevron-bottom")
      }
      else if($("#showSavedRules").hasClass("cui-chevron-bottom")){
        $("#showSavedRules").removeClass("cui-chevron-bottom")
        $("#showSavedRules").addClass("cui-chevron-top")
      }

      if($(".savedrulesdisplay").hasClass("d-none")){
        $(".savedrulesdisplay").removeClass("d-none")
        $(".savedrulesdisplay").addClass("d-block")
      }
      else if($(".savedrulesdisplay").hasClass("d-block")){
        $(".savedrulesdisplay").removeClass("d-block")
        $(".savedrulesdisplay").addClass("d-none")
      }
    }

    

  }

  componentDidMount(){
    this.getCategoriesData()
    this.getSubCategoriesData()
    this.getSeasonsData()
    this.getGeneralRules()
    this.getOutfitRules()
    this.getCustomerRules()
    this.getFeedback()
    
  }

  componentDidUpdate(prevProps, prevState){

    if (prevState.triggerFeedback !== this.state.triggerFeedback) {
      this.handleSendFeedback()
    }

    if (prevState.category !== this.state.category) {
      this.setState({btnFreq: false})
    }
  }

  selectUserID(e){
    const listItems = e.target.childNodes
    const usuario = listItems[2].nodeValue
    
    console.log(usuario);
    const elements = Array.from(e.target.parentElement.childNodes)
    elements.map( el => el.classList.remove("selected"))
    // e.target.parentElement.childNodes.classList.add("selected")
    // siblings.className -= ' selected'
    e.target.classList.add("selected")
    this.setState({UserID: usuario})

  }

  selectGeneralRules(e){
      
    const rules = e.target.parentElement.childNodes
    
    
      const boxes = rules[1].innerHTML || null
      const minProd = rules[2].innerHTML || null
      const maxProd = rules[3].innerHTML || null
      const position = rules[4].innerHTML || null
      const frequency = rules[6].innerHTML || null
      const toCart = rules[7].innerHTML || null
    
    
    
    this.setState({
      nboxes: boxes, minproducts: minProd, maxproducts: maxProd, position: position, CategoryFrequency: JSON.parse(frequency), addoutfit: toCart
    })

    this.forceUpdate(()=>this.state.nboxes)
    this.forceUpdate(()=>this.state.minproducts)
    this.forceUpdate(()=>this.state.maxproducts)
    this.forceUpdate(()=>this.state.position)
    this.forceUpdate(()=>this.state.CategoryFrequency)
    this.forceUpdate(()=>this.state.addoutfit)

    console.log(rules)

    const rulesArray = Array.from(e.target.parentNode.parentNode.childNodes)
    rulesArray.map( el => el.classList.remove("selected"))
    e.target.parentNode.classList.add("selected") 

    if($(".check-general").hasClass("d-none")){
      $(".check-general").removeClass("d-none")
      $(".check-general").addClass("d-block")
    } 

  }

  selectOutfitRules(e){
      
    const rules = e.target.parentElement.childNodes
    
    // const key = rules[0].innerHTML
    const season = rules[1].innerHTML
    const mincost = rules[2].innerHTML
    const maxcost = rules[3].innerHTML
    
    
    this.setState({season: season, minprice: mincost, maxprice: maxcost})

    this.forceUpdate(()=>this.state.season)
    this.forceUpdate(()=>this.state.minprice)
    this.forceUpdate(()=>this.state.maxprice)

    const rulesArray = Array.from(e.target.parentNode.parentNode.childNodes)
    rulesArray.map( el => el.classList.remove("selected"))
    e.target.parentNode.classList.add("selected") 

    if($(".check-outfit").hasClass("d-none")){
      $(".check-outfit").removeClass("d-none")
      $(".check-outfit").addClass("d-block")
    } 

  }

  selectCustomerRules(e){
      
    const rules = e.target.parentElement.childNodes
    
    // const key = rules[0].innerHTML
    const bodyshape = rules[1].innerHTML
    const minage = rules[2].innerHTML
    const maxage = rules[3].innerHTML
    const size = rules[4].innerHTML
    const location = rules[5].innerHTML
    const gender = rules[6].innerHTML
    
    this.setState({
      bodyshape: bodyshape, minage: minage, maxage: maxage, size: size, location: location, gender: gender
    })

    this.forceUpdate(()=>this.state.bodyshape)
    this.forceUpdate(()=>this.state.minage)
    this.forceUpdate(()=>this.state.maxage)
    this.forceUpdate(()=>this.state.size)
    this.forceUpdate(()=>this.state.location)
    this.forceUpdate(()=>this.state.gender)

    
    const rulesArray = Array.from(e.target.parentNode.parentNode.childNodes)
    rulesArray.map( el => el.classList.remove("selected"))
    e.target.parentNode.classList.add("selected") 

  }

  removeCustomerRule(e){

    const rule_id = e.target.id
    
    this.deleteCustomerRules(rule_id)
    this.getCustomerRules()

  }

  removeAllCustomerRules(){
    this.deleteAllCustomerRules()
    this.getCustomerRules()

  }

  removeOutfitRule(e){

    const rule_id = e.target.id
    
    this.deleteOutfitRules(rule_id)
    this.getOutfitRules()

  }

  removeAllOutfitRules(){
    this.deleteAllOutfitRules()
    this.getOutfitRules()

  }

  removeGeneralRule(e){

    const rule_id = e.target.id
    
    this.deleteGeneralRules(rule_id)
    this.getGeneralRules()

  }

  removeAllGeneralRules(){
    this.deleteAllGeneralRules()
    this.getGeneralRules()

  }

  handleSend(e){
    e.preventDefault()

    console.log("UserID: ",this.state.UserID, "nBoxes: ", this.state.nboxes, "minProducts: ",this.state.minproducts,"maxProducts: ",this.state.maxproducts,"season: ","all","minCost: ", this.state.minprice,"maxCost: ", this.state.maxprice,"freqString: ",this.state.CategoryFrequency)
    this.setState({boxResult: [], boxesResult: [], disableSendFeedback: false, disableSend: true })
    this.getData()
    this.getDataForFeedback()
  }

  handleSendFeedback(){
    console.log(`BOX: ${this.state.boxResult} FEEDBACK: ${this.state.feedback} COMMENT: ${this.state.feedbackComment}`)
    // this.saveBox()
    this.saveFeedback()
    this.setState({disableSendFeedback: true, disableSend: false})
    this.getDataForFeedback()
    document.querySelector("#feedback-textarea").value = ""
  }

  handleSubmitCustomer(e){
    e.preventDefault()
    if(this.state.UserList > 0){
      this.state.UserList === 0
    }
    
    if(this.state.gender === "" || this.state.bodyshape === "" || this.state.size === "" || this.state.location === ""){

      alert("You must fill all of customer rules before submit")
    }
    else{
      
      this.getCustomerList()
      console.log(this.state.savedCustomerRules)
      this.saveCustomerRules()
      this.getCustomerRules()
      this.setState({alertOn: true})
      
    }
  }

  handleSubmitOutfit(e){
    e.preventDefault()

    this.saveOutfitRules()
    this.getOutfitRules()
    if($(".check-outfit").hasClass("d-none")){
      $(".check-outfit").removeClass("d-none")
      $(".check-outfit").addClass("d-block")
    } 
  }

  handleSubmitGeneral(e){
    e.preventDefault()

    this.saveGeneralRules()
    
    this.getGeneralRules()
    if($(".check-general").hasClass("d-none")){
      $(".check-general").removeClass("d-none")
      $(".check-general").addClass("d-block")
    }
  }

  handleChange(e){

    if(e.target.name === "btnFreq"){
      
      e.preventDefault()
      
      console.log(e.target.parentNode.children[1].id)
      
      console.log(`${this.state.CategoryFrequency.length}, ${this.state.maxproducts}`)
      if(this.state.CategoryFrequency.length >= this.state.maxproducts){
        
        const buttonArray = Array.from(document.getElementsByClassName("btn-assign"))
        const maxProdInput = document.querySelector("input[name='maxproducts']")
        
        maxProdInput.setAttribute("readOnly", "true")
        // buttonArray.map(el => el.setAttribute("disabled", "true"))
        
        return
      }
      else{
        let key = e.target.parentNode.children[1].id
        let value = e.target.parentNode.children[1].value
        
        this.addCategoryFrequency(key, value)
        document.querySelector("input[name='CategoryFrequency']").value = "0"
        this.setState({btnFreq: true})
      }
      
    }
    else if(e.target.name === "feedback"){
      this.setState({[e.target.name]: e.target.value})
      this.setState((prevState)=>({triggerFeedback: !prevState.triggerFeedback}))
      
    }
    else{
      this.setState({[e.target.name]: e.target.value})
    }
    
  }

  render() {
    
    const minOfMax = parseInt(this.state.minproducts)
    const MaxOfMin = parseInt(this.state.maxproducts)

    const minOfMaxA = parseInt(this.state.minage)+1
    const MaxOfMinA = parseInt(this.state.maxage)-1

    const minOfMaxP = parseInt(this.state.minprice)+1
    const MaxOfMinP = parseInt(this.state.maxprice)-1


    return (
      <React.Fragment>
        <div className="container">
          <div className="row">
            {/* GENERAL RULES */}
            <div id="generalrules" className="card my-3 shadow col-12">
              <div className="card-body">
                <h2><span title="show/hide rules" onClick={this.showGeneralRules} id="showGeneralRules" className="iconToggler icons rounded text-white py-2 px-2 font-3xl mr-4 mt-5 cui-chevron-bottom"></span>General Rules</h2>
                <div className="generalrulesdisplay d-block">
                <div className="my-2 mt-4 px-2 py-2">
                  <form className="form-inline">
                  <div className="form-group">
                  <div className="col-12 col-sm-10 col-md-6">
                  <label className=" mx-2 text-uppercase font-weight-bold text-muted">Start Date:</label>
                    <select onChange={this.handleChange} className="form-control" name="startYear">
                      <option value="2019">2019</option>
                      <option value="2018">2018</option>
                      <option value="2017">2017</option>
                      <option value="2016">2016</option>
                      <option value="2015">2015</option>
                      <option value="2014">2014</option>
                    </select>
                    <select onChange={this.handleChange} className="form-control" name="startMonth">
                      <option value="1">Enero</option>
                      <option value="2">Febrero</option>
                      <option value="3">Marzo</option>
                      <option value="4">Abril</option>
                      <option value="5">Mayo</option>
                      <option value="6">Junio</option>
                      <option value="7">Julio</option>
                      <option value="8">Agosto</option>
                      <option value="9">Septiembre</option>
                      <option value="10">Octubre</option>
                      <option value="11">Noviembre</option>
                      <option value="12">Diciembre</option>                
                    </select>
                    <select onChange={this.handleChange} className="form-control" name="startDay">
                    <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                      <option value="13">13</option>
                      <option value="14">14</option>
                      <option value="15">15</option>
                      <option value="16">16</option>
                      <option value="17">17</option>
                      <option value="18">18</option>
                      <option value="19">19</option>
                      <option value="20">20</option>
                      <option value="21">21</option>
                      <option value="22">22</option>
                      <option value="23">23</option>
                      <option value="24">24</option>
                      <option value="25">25</option>
                      <option value="26">26</option>
                      <option value="27">27</option>
                      <option value="28">28</option>
                      <option value="29">29</option>
                      <option value="30">30</option>
                      <option value="31">31</option>
                    </select>
                    </div>
                    <div className="col-12 col-sm-10 col-md-6">
                      <label className="mx-2 text-uppercase font-weight-bold text-muted">Expiration Date:</label>
                      <select onChange={this.handleChange} className="form-control" name="finalYear">
                        <option value="2019">2019</option>
                        <option value="2018">2018</option>
                        <option value="2017">2017</option>
                        <option value="2016">2016</option>
                        <option value="2015">2015</option>
                        <option value="2014">2014</option>
                      </select>
                      <select onChange={this.handleChange} className="form-control" name="finalMonth">
                        <option value="1">Enero</option>
                        <option value="2">Febrero</option>
                        <option selected value="3">Marzo</option>
                        <option value="4">Abril</option>
                        <option value="5">Mayo</option>
                        <option value="6">Junio</option>
                        <option value="7">Julio</option>
                        <option value="8">Agosto</option>
                        <option value="9">Septiembre</option>
                        <option value="10">Octubre</option>
                        <option value="11">Noviembre</option>
                        <option value="12">Diciembre</option>                  
                      </select>
                      <select onChange={this.handleChange} className="form-control"  name="finalDay">
                      <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                        <option value="21">21</option>
                        <option value="22">22</option>
                        <option value="23">23</option>
                        <option value="24">24</option>
                        <option value="25">25</option>
                        <option value="26">26</option>
                        <option value="27">27</option>
                        <option value="28">28</option>
                        <option value="29">29</option>
                        <option value="30">30</option>
                        <option value="31">31</option>
                      </select>
                    </div>
                    </div>
                  </form>
                </div>
                <div className="my-4 card-rule rounded px-2 py-2">
                    <div className="py-3 form-inline">
                      <label className="mx-2 mr-4 text-uppercase font-weight-bold text-muted">boxes:</label>
                      <label className="col-12 col-sm-1 text-capitalize font-weight-bold">amount</label>
                      <input className="mr-2 ml-1 form-control" onChange={this.handleChange} name="nboxes" type="number" min="1" max="7" value={this.state.nboxes}/>
                    </div>
                    <hr />
                    <div className="py-3 form-inline">
                      <label className="mx-2 mr-4 text-uppercase font-weight-bold text-muted">items in a box:</label>
                      <label className="col-12 col-sm-1 text-capitalize font-weight-bold">min</label>
                      <input className="mr-2 ml-1 form-control" onChange={this.handleChange} name="minproducts" type="number" min="2" max={MaxOfMin} value={this.state.minproducts}/>
                      <label className="col-12 col-sm-1 text-capitalize font-weight-bold">max</label>
                      <input className="mr-2 ml-1 form-control" onChange={this.handleChange} name="maxproducts" type="number" min={minOfMax} max="5" value={this.state.maxproducts}/>
                    </div>
                    <hr />
                    {/* <div className="py-3 form-inline">
                      <label className="mx-2 mr-4 text-uppercase font-weight-bold text-muted">position:</label>
                      <select onChange={this.handleChange} name="position" className="form-control">
                        <option value="">Select Position</option>
                        { this.state.selectPosition.map((value, index)=>{
                          return <option key={index} value={value}>{value}</option>
                        })}
                      </select>
                    </div>
                    <hr /> */}
                    <div className="py-3 form-inline">
                      <label className="mx-2 mr-4 text-uppercase font-weight-bold text-muted">frequency:</label>
                      <select onChange={this.handleChange} name="category" className="col-12 col-sm-3 form-control">
                        <option value="">Select Category</option>
                        {this.state.selectCategory.map((value,index) => { 
                          return <option key={index}>{value}</option>
                        })}
                      </select>
                    </div>
                      {this.state.category && 
                        <ul className=" list-group">
                          <li className="bg-lightgrey list-group-item">
                            <h4><span className="badge badge-primary">{this.state.category}</span></h4>
                            <div className="py-3 form-inline">
                              <label className="my-auto col-12 col-sm-1 text-capitalize font-weight-bold">every</label>
                              <input className="freq mr-2 ml-1 form-control" id={this.state.category} name="CategoryFrequency" type="number" min="1" max="12"/>
                              <label className="my-auto col-12 col-sm-1 text-capitalize font-weight-bold">months</label>
                              <button onClick={this.handleChange} disabled={this.state.btnFreq} id="btnFreq" name="btnFreq" className=" ml-4 btn-assign btn btn-small btn-secondary">Assign</button>
                            </div>
                          </li>
                        </ul>
                      }
                      {this.state.CategoryFrequency.length > 0 &&
                        <ol>
                          {this.state.CategoryFrequency.map((value, index) => {
                            return (
                              <li className="lista-viume" key={index}>
                                <i onClick={() => this.removeCategoryFrequency(value)} className="fa fa-window-close" aria-hidden="true"></i>
                                {this.frequencyDisplay(JSON.stringify(value))}
                              </li>
                            )
                          })}
                        </ol>
                      }
                    {/* <div className=" overflow-auto" style={{maxHeight: "250px"}}>
                      <ul className=" list-group">
                        {this.state.selectCategory ? this.state.selectCategory.map((value, index)=>{
                          return <li className="bg-lightgrey list-group-item" key={index}>
                                    <h4><span className="badge badge-primary">{value}</span></h4>
                                    <div className="py-3 form-inline">
                                      <label className="my-auto col-12 col-sm-1 text-capitalize font-weight-bold">every</label>
                                      <input className="freq mr-2 ml-1 form-control" id={value} name="CategoryFrequency" type="number" min="1" max="12"/>
                                      <label className="my-auto col-12 col-sm-1 text-capitalize font-weight-bold">months</label>
                                      <button onClick={this.handleChange} name="btnFreq" className=" ml-4 btn-assign btn btn-small btn-secondary">Assign</button>
                                    </div>
                                  </li>
                        }) : null}
                      </ul>
                    </div> */}
                    
                    {/* <hr />
                    <div className="py-3 form-inline">
                      <label className="mx-2 mr-4 text-uppercase font-weight-bold text-muted">add outfit to cart:</label>
                      <div className="ml-2 form-check form-check-inline">
                        <label className="form-check-label mr-2" htmlFor="yes">Yes</label>
                        <input onChange={this.handleChange} className="form-check-input" type="radio"  name="addoutfit" id="yes" value="true"/>
                      </div>
                      <div className="ml-2 form-check form-check-inline">
                        <label className="form-check-label mr-2" htmlFor="no">No</label>
                        <input onChange={this.handleChange} className="form-check-input" type="radio" name="addoutfit" id="no" value="false"/>
                      </div>
                    </div> */}
                    
                </div>
                
                  <div title="confirm general rules" className="my-4 form-inline px-2 py-4">
                    <button onClick={this.handleSubmitGeneral} className="btn btn-lg btn-success">Confirm</button>
                    <i className="ml-4 d-none check-general far fa-check-circle"></i>
                  </div>
                
                  
                  </div>
              </div>
            </div>
          </div>
          <div className="row">


            
            {/* OUTFIT RULES */}
            <div id="outfitrules" className="card mb-3 shadow col-12">
              <div className="card-body">
                <h2><span title="show/hide rules" onClick={this.showOutfitRules} id="showOutfitRules" className="iconToggler icons text-white py-2 px-2 font-3xl mr-4 mt-5 cui-chevron-bottom"></span>Outfit Rules </h2>
                <div className="outfitrulesdisplay d-block">
                  <form>
                  <div title="" className="card-rule my-4 form-inline rounded px-2 py-4">
                    <label className="mx-2 mr-4 text-uppercase font-weight-bold text-muted">season:</label>
                    <select onChange={this.handleChange} name="season" className="form-control">
                      <option value="all">All Seasons</option>
                      { this.state.selectSeason.map((value, index) => {
                        return <option key={index}>{value}</option>
                      })}
                    </select>
                  </div>
                  <div title="" className="card-rule my-4 form-inline rounded px-2 py-4">
                    <label className="mx-2 mr-4 text-uppercase font-weight-bold text-muted">price:</label>
                    <label className="col-12 col-sm-1 text-capitalize font-weight-bold">min</label>
                    <input className="mr-2 ml-1 form-control" onChange={this.handleChange} name="minprice" type="number" min="0" max={MaxOfMinP} value={this.state.minprice}/>
                    <label className="col-12 col-sm-1 text-capitalize font-weight-bold">max</label>
                    <input className="mr-2 ml-1 form-control" onChange={this.handleChange} name="maxprice" type="number" min={minOfMaxP} max="250" value={this.state.maxprice}/>
                  </div>
                  <div title="boost specific category" className="card-rule my-4 form-inline rounded px-2 py-4">
                    <label className="mx-2 mr-4 text-uppercase font-weight-bold text-muted">category:</label>
                    <select onChange={this.handleChange} name="" className="form-control">
                      <option value="">Select Category</option>
                      {this.state.selectCategory.map((value,index) => { 
                        return <option key={index}>{value}</option>
                      })}
                    </select>
                  </div>
                  <div title="boost specific subcategory" className="card-rule my-4 form-inline rounded px-2 py-4">
                    <label className="mx-2 mr-4 text-uppercase font-weight-bold text-muted">subcategory:</label>
                    <select onChange={this.handleChange} name="subcategory" className="form-control">
                      <option value="">Select Subcategory</option>
                      {this.state.selectSubcategory.map((value,index) => { 
                        return <option key={index}>{value}</option>
                      })}
                    </select>
                  </div>
                  <div title="boost specific color" className="card-rule my-4 form-inline rounded px-2 py-4">
                    <label className="mx-2 mr-4 text-uppercase font-weight-bold text-muted">color:</label>
                    <select onChange={this.handleChange} name="color" className="form-control">
                      <option value="">Select Color</option>
                      {this.state.selectColor.map((value,index) => { 
                        return <option key={index}>{value}</option>
                      })}
                    </select>
                  </div>
                  <div title="combine items from different ocassions" className="card-rule my-4 form-inline rounded px-2 py-4">
                    <label className="mx-2 mr-4 text-uppercase font-weight-bold text-muted">ocassion:</label>
                    <select onChange={this.handleChange} name="ocassion" className="form-control">
                      <option value="">Select Ocassion</option>
                      <option value=""></option>
                      <option value=""></option>
                      <option value=""></option>
                      <option value=""></option>
                    </select>
                  </div>
                  <div title="boost specific items" className="card-rule my-4 form-inline rounded px-2 py-4">
                    <label className="mx-2 mr-4 text-uppercase font-weight-bold text-muted">item:</label>
                    <select onChange={this.handleChange} name="item" className="form-control">
                      <option value="">Select an Item</option>
                      <option value=""></option>
                      <option value=""></option>
                      <option value=""></option>
                    </select>
                  </div>
                  <div title="" className="card-rule my-4 form-inline rounded px-2 py-4">
                    <label className="mx-2 mr-4 text-uppercase font-weight-bold text-muted">brands:</label>
                    <select onChange={this.handleChange} name="brands" className="form-control">
                      <option value="">Select a Brand</option>
                      <option value=""></option>
                      <option value=""></option>
                      <option value=""></option>
                    </select>
                  </div>
                  <div title="confirm outfit rules" className="my-4 form-inline px-2 py-4">
                    <button onClick={this.handleSubmitOutfit} className="btn btn-lg btn-success">Confirm</button>
                    <i className="ml-4 d-none far check-outfit fa-check-circle"></i>
                  </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div className="row">



            {/* CUSTOMER RULES */}
            <div id="customerrules" className="card mb-3 shadow col-12">
              <div className="card-body">
                <h2><span title="show/hide rules" onClick={this.showCustomerRules} id="showCustomerRules" className="iconToggler icons text-white py-2 px-2 font-3xl mr-4 mt-5 cui-chevron-bottom"></span>Customer Rules</h2>
                <div className="customerrulesdisplay d-block">
                <form >
                <div title="" className="card-rule my-4 form-inline rounded px-2 py-4">
                  <label className="mx-2 mr-4 text-uppercase font-weight-bold text-muted">body shape:</label>
                  <select onChange={this.handleChange} name="bodyshape" className="form-control">
                    <option value="">Select Body Shape</option>
                    { this.state.selectBodyshape.map((value, index)=>{
                      return <option key={index} value={value}>{value}</option>
                    })}
                  </select>
                </div>
                <div title="" className="card-rule my-4 form-inline rounded px-2 py-4">
                  <label className="mx-2 mr-4 text-uppercase font-weight-bold text-muted">age:</label>
                  <label className="col-12 col-sm-1 text-capitalize font-weight-bold">min</label>
                  <input className="mr-2 ml-1 form-control" onChange={this.handleChange} name="minage" type="number" min="18" max={MaxOfMinA} value={this.state.minage}/>
                  <label className="col-12 col-sm-1 text-capitalize font-weight-bold">max</label>
                  <input className="mr-2 ml-1 form-control" onChange={this.handleChange} name="maxage" type="number" min={minOfMaxA} max="65" value={this.state.maxage}/>
                </div>
                <div title="" className="card-rule my-4 form-inline rounded px-2 py-4">
                  <label className="mx-2 mr-4 text-uppercase font-weight-bold text-muted">gender:</label>
                  <select onChange={this.handleChange} name="gender" className="form-control">
                  <option value="">Select Gender</option>
                    { this.state.selectGender.map((value, index)=>{
                      return <option key={index} value={value==="Woman" ? "M" : "H"}>{value}</option>
                    })}
                  </select>
                </div>
                <div title="" className="card-rule my-4 form-inline rounded px-2 py-4">
                  <label className="mx-2 mr-4 text-uppercase font-weight-bold text-muted">size:</label>
                  <select onChange={this.handleChange} name="size" className="form-control">
                  <option value="">Select Size</option>
                    { this.state.selectSize.map((value, index)=>{
                      return <option key={index} value={value}>{value}</option>
                    })}
                  </select>
                </div>
                <div title="" className="card-rule my-4 form-inline rounded px-2 py-4">
                  <label className="mx-2 mr-4 text-uppercase font-weight-bold text-muted">location:</label>
                  <select onChange={this.handleChange} name="location" className="form-control">
                    <option value="">Select Location</option>
                    { this.state.selectLocation.map((value, index)=>{
                      return <option key={index} value={value}>{value}</option>
                    })}
                  </select>
                </div>
              </form>
              <div  className="my-4 form-inline px-2 py-4">
                  <button title="confirm customer rules" onClick={this.handleSubmitCustomer} className="btn btn-lg btn-success">Confirm</button>
                </div>
                {this.state.UserList.length>0 && <h4 className="mb-4">Customer Group</h4>}
                <div className="overflow-auto" style={{maxHeight: "200px"}}>
                  <ul className="list-group">
                    {this.state.UserList && this.state.UserList.map((value, index)=>{
                      return (
                        <li onClick={this.selectUserID} className="list-group-item selectProductRow" name={value} key={index}><span className="text-muted font-weight-bold">User ID:</span> {value}</li>
                      )
                    })}
                  </ul>
                </div>
                {this.state.loading ? 
                  <div className="row">
                    <div className="text-center col-12">
                      <i className="font-size-2 fa fa-refresh fa-spin"></i>
                    </div>
                  </div> : null}
                {(this.state.UserList.length === 0 && this.state.alertOn && !this.state.loading) ? <div role="alert" className="alert alert-warning my-2 ml-3 font-style-bold text-uppercase">0 Customers found</div> : null}
              </div>
            </div>
          </div>
          </div>

          <div className={this.state.btnSendDisplay + " row" }>
            <div className="card mb-3 py-4 shadow col-12">
              <button onClick={this.handleSend} className="my-3 btn btn-lg font-weight-bold btn-success">GET RECOMMENDATION</button>
              {this.state.boxesResult.length>0 && 
                <div className="pl-3 mx-auto savedBox py-3 col-12">
                  <h3>Outfits for user: {this.state.UserID}</h3>
                  {this.state.boxesResult.map((value) => {
                    return (
                      <table className="mt-4 ml-2 table table-responsive-xxs">
                        <thead>
                          <tr>
                            <th>CATEGORY</th>
                            <th>PRODUCT ID</th>
                            <th>PRICE</th>
                            <th>COLOR</th>
                            <th>IMAGE</th>
                          </tr>
                        </thead>
                        <tbody>

                        {value.map((value, index) => {
                            
                            return(
                            Array.isArray(value) && (
                              <tr key={index}>
                                <td>{value[0]}</td>
                                <td>{value[1]}</td>
                                <td>{value[2]}€</td>
                                <td>{value[3]}</td>
                                <td></td>
                              </tr>))
                              
                            
                            
                        }
                        )}
                          
                        </tbody>
                      </table>
                    )
                  })}
              </div>
              }
              {
                this.state.noResults && <div role="alert" className="alert alert-success my-2 ml-3 font-style-bold text-uppercase">0 Results</div>
              }
            
              
            </div>
          </div>
          <div className={this.state.feedbackDisplay + " row" }>
            {/* FEEDBACK RULES */}
            <div id="feedbackrules" className="card mb-3 shadow col-12">
              <div className="card-body">
                <h2 className="mb-4">Feedback Rules</h2>
                  <div className="row">{
                    (this.state.boxResult.length>0) && 
                      <div className="pl-3 border savedBox py-3 col-12">
                        <h3>Outfit to feedback</h3>
                          {this.state.boxResult.map((value) => {
                            return (
                              <table className="mt-4 ml-2 table table-responsive-xxs">
                                <thead>
                                  <tr>
                                  <th>CATEGORY</th>
                                  <th>PRODUCT ID</th>
                                  <th>PRICE</th>
                                  <th>COLOR</th>
                                  <th>IMAGE</th>
                                  </tr>
                                </thead>
                                <tbody>

                                {value.map((value, index) => {
                                    
                                    return(
                                    Array.isArray(value) && (
                                      <tr key={index}>
                                        <td>{value[0]}</td>
                                        <td>{value[1]}</td>
                                        <td>{value[2]}€</td>
                                        <td>{value[3]}</td>
                                        <td></td>
                                      </tr>))
                                }
                                )}
                              </tbody>
                            </table>
                            )
                          })}
                          </div>
                          }
                          {
                            this.state.noFeedbackResults && <div role="alert" className="alert alert-success my-2 ml-3 font-style-bold text-uppercase">0 Results</div>
                          }
                  {
                    !this.state.noFeedbackResults && 
                    
                    <div className="col-12">
                      <div>
                        <label className="text-muted mt-2 font-weight-bold" htmlFor="feedback-textarea">Give some feedback comment</label>
                        <textarea onChange={this.handleChange} name="feedbackComment" className="form-control" id="feedback-textarea" rows="3"></textarea>
                      </div>
                      <div className="my-4 btn-group">
                        <button onClick={this.handleChange} name="feedback" value="1" className="btn btn-lg btn-success">Liked</button>
                        <button onClick={this.handleChange} name="feedback" value="0" className="btn btn-lg btn-danger">Disliked</button>
                      </div>
                      {/* <button disabled={this.state.disableSendFeedback} onClick={this.handleSendFeedback} className="ml-4 btn btn-lg font-weight-bold btn-info">SEND FEEDBACK</button> */}
                  </div>
                  }
                  
              </div>
            </div>
          </div>
          </div>


          <div className="row">
            {/* SAVED RULES */}
            <div id="savedrules" className="card mb-3 shadow col-12">
              <div className="card-body">
                <h2><span title="show/hide rules" onClick={this.showSavedRules} id="showSavedRules" className="iconToggler icons rounded text-white py-2 px-2 font-3xl mr-4 mt-5 cui-chevron-bottom"></span>Saved Rules</h2>
                
                  <div className="row">
                  <div className="savedrulesdisplay d-block">{
                    (this.state.savedCustomerRules.length>0 || this.state.savedGeneralRules.length>0 || this.state.savedOutfitRules.length>0 ) ? 
                      <React.Fragment>
                        <div className="pl-3 my-4 rounded border savedBox py-3 col-12">
                          <h3>General Rules <button type="button" onClick={this.removeAllGeneralRules} id="all" title="remove all" className="btn ml-2 rmv-btn btn-warning"><i className="cui-trash h4 text-gray-900"></i></button></h3>
                          <table className="mt-4 table table-striped table-responsive-xxs table-bordered">
                          <thead>
                            <tr>
                              <th>ID</th>
                              <th>BOXES</th>
                              <th>MIN PRODUCTS</th>
                              <th>MAX PRODUCTS</th>
                              <th>POSITION</th>
                              <th>FREQUENCY</th>
                              <th>ADD TO CART</th>
                              <th></th>
                            </tr>
                            
                          </thead>
                          <tbody>
                            { this.state.savedGeneralRules.map((value, index) => { return (
                              <tr onClick={this.selectGeneralRules} className="selectProductRow" key={index}>
                                <td>{value.RULE_ID}</td>
                                <td>{value.BOXES}</td>
                                <td>{value.MIN_PROD}</td>
                                <td>{value.MAX_PROD}</td>
                                <td>{value.POSITION}</td>
                                <td>{this.frequencyDisplay(value.FREQUENCY)}</td>
                                <td className="d-none">{value.FREQUENCY}</td>
                                <td>{value.ADD_CART}</td>
                                <td><i onClick={this.removeGeneralRule} id={value.RULE_ID} title="remove" className="cui-trash trash h3 text-gray-900"></i></td>
                            </tr>
                            ) })
                            
                            }
                          </tbody>
                          </table>
                        </div>
                        <div className="pl-3 my-4 rounded border savedBox py-3 col-12">
                          <h3>Outfit Rules <button type="button" onClick={this.removeAllOutfitRules} id="all" title="remove all" className="btn ml-2 rmv-btn btn-warning"><i className="cui-trash h4 text-gray-900"></i></button></h3>
                          <table className="mt-4 table table-striped table-responsive-xxs table-bordered">
                          <thead>
                            <tr>
                              <th>ID</th>
                              <th>SEASON</th>
                              <th>MIN PRICE</th>
                              <th>MAX PRICE</th>
                              <th></th>
                            </tr>
                            
                          </thead>
                          <tbody>
                            { this.state.savedOutfitRules.map((value, index) => { return (
                              <tr onClick={this.selectOutfitRules} className="selectProductRow" key={index}>
                                <td>{value.RULE_ID}</td>
                                <td>{value.SEASON}</td>
                                <td>{value.MIN_PRICE}</td>
                                <td>{value.MAX_PRICE}</td>
                                <td><i onClick={this.removeOutfitRule} id={value.RULE_ID} title="remove" className="cui-trash trash h3 text-gray-900"></i></td>
                            </tr>
                            ) })
                            
                            }
                          </tbody>
                          </table>
                        </div>
                        <div className="pl-3 my-4 rounded border savedBox py-3 col-12">
                          <h3>Customer Rules <button type="button" onClick={this.removeAllCustomerRules} title="remove all" className="btn ml-2 rmv-btn btn-warning"><i className="cui-trash h4 text-gray-900"></i></button></h3>
                          <table className="mt-4 table table-striped table-responsive-xxs table-bordered">
                          <thead>
                            <tr>
                              <th>ID</th>
                              <th>BODY SHAPE</th>
                              <th>MIN AGE</th>
                              <th>MAX AGE</th>
                              <th>SIZE</th>
                              <th>LOCATION</th>
                              <th>GENDER</th>
                              <th></th>
                            </tr>
                            
                          </thead>
                          <tbody>
                            { this.state.savedCustomerRules.map((value, index) => { return (
                              <tr onClick={this.selectCustomerRules} className="selectProductRow" key={index}>
                                <td>{value.RULE_ID}</td>
                                <td>{value.BODY_SHAPE}</td>
                                <td>{this.getAge(value.FECHA_NACIMIENTO_MAX)}</td>
                                <td>{this.getAge(value.FECHA_NACIMIENTO_MIN)}</td>
                                <td>{value.SIZE}</td>
                                <td>{value.PROV_CLIENTE}</td>
                                <td>{value.GENDER}</td>
                                <td><i onClick={this.removeCustomerRule} id={value.RULE_ID} title="remove" className="cui-trash trash h3 text-gray-900"></i></td>
                            </tr>
                            ) })
                            
                            }
                          </tbody>
                          </table>
                        </div>
                      </React.Fragment>
                      :<div role="alert" className="alert alert-success my-2 ml-3 font-style-bold text-uppercase">No saved Rules</div>
                  }
              </div>
              </div>
            </div>
          </div>
        </div>
        </div>
      </React.Fragment>
    )
  }
}

export default ConfigRules
