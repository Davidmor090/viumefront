import axios from 'axios';

const USER_TOKEN = localStorage.getItem("accessToken")
const AuthStr = 'Bearer ' + USER_TOKEN;

const get = async (url = '', params = '') => {
  return await new Promise( (resolve, reject) => {
    axios.get(url + params, { 'headers': { 'Authorization': AuthStr, 'X-XSS-Protection': 0 } })
      .then(res => {
        resolve(res.data);
      }).catch(error => {
        reject(new Error(error));
      });
  });
}

export default get;