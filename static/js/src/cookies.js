const COOKIE_DAYS = 7
const DAY_SECONDS = 86400000

const setCookie = (name, value, days = COOKIE_DAYS, path = '/') => {
  const expires = new Date(Date.now() + days * DAY_SECONDS).toUTCString()
  value = encodeURIComponent(value)
  document.cookie = `${name}=${value}; expires=${expires}; path=${path}`
}

const getCookie = (name) =>
  document.cookie.split('; ').reduce((r, v) => {
    const [n, ...val] = v.split('=')
    return n === name ? decodeURIComponent(val.join('=')) : r
  }, '')


const deleteCookie = (name, path) => setCookie(name, '', -1, path)

export {
  setCookie,
  getCookie,
  deleteCookie
}
