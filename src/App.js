import '@coreui/coreui';

import '@coreui/coreui/scss/coreui.scss';
import '@coreui/icons/scss/coreui-icons.scss';
import 'flag-icon-css/sass/flag-icon.scss';
import 'simple-line-icons/scss/simple-line-icons.scss';
import 'font-awesome/scss/font-awesome.scss';
import './scss/style.scss';

import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { UserProvider } from './UserContext';

import PrivateRoute from './components/PrivateRoute';
import Home from './components/Home/Home';
import Details from './components/Details/Details';
import Login from './components/Login/Login';
import ConfigRecommender from './components/Configuration/ConfigRecommender';
import ContactUs from './components/ContactUs/ContactUs';
import HelpDesk from './components/HelpDesk/HelpDesk';
import Profile from './components/Profile/Profile';
import FAQ from './components/HelpDesk/FAQ';
import Documentation from './components/HelpDesk/Documentation';
import TicketSystem from './components/HelpDesk/TicketSystem';
import Register from './components/Register';



class App extends Component {

  constructor(props) {
    super(props)

    this.state = {
      user:
      {
        name: 'Tania', accessGranted: false, loggedIn: false, email: 'tania@gmail.co', age: 27
      },

    }
  }

  render() {
    const user = this.state.user;
    const setAccess = this.setAccess
    return (
      <UserProvider value={user}>
        <Router>
          <PrivateRoute exact path="/" component={Home} />
          <PrivateRoute exact path="/contact-us" component={ContactUs} />
          <PrivateRoute exact path="/help-desk" component={HelpDesk} />
          <PrivateRoute exact path="/faqs" component={FAQ} />
          <PrivateRoute exact path="/documentation" component={Documentation} />
          <PrivateRoute exact path="/contact-support" component={TicketSystem} />
          <PrivateRoute exact path="/details" component={Details} />
          <PrivateRoute exact path="/custom-recommender" component={ConfigRecommender} />
          <PrivateRoute exact path="/my-profile" component={Profile} />
          <Route path="/login" component={Login} />
          <Route path="/register" component={Register} />
        </Router>
      </UserProvider>
    );
  }
}

export default App


