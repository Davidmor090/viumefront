import React, { Component } from 'react';
// import axios from 'axios';
import api from '../../services'
// import ProductTag from './ProductTag';

// import UploadImage from './UploadImage';


class MainDetails extends Component {
  
  constructor(){
    super()
    this.state={
      
      fileUrl: "",
      targetImg: '0',
      selectedFile: null,
      targetUrl: '',
      targetTags: {},
      color: [],
      colorName: [],
      tagging: false,
      getTagsButton: false
      
    }
    
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleDeleteOne = this.handleDeleteOne.bind(this);
    this.handleDeleteAll = this.handleDeleteAll.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
    this.handleGetTags = this.handleGetTags.bind(this);
    this.handleUploadImage = this.handleUploadImage.bind(this);

    this.tagIt = async () => {
      if(this.state.targetUrl != ""){
        this.setState({targetTags: {}, color: []})
        this.setState({tagging: true})
        console.log("tagging: ",this.state.tagging)
        const body = { "IMG_URL": this.state.targetUrl }
        const res = await api.post("http://206.189.166.235:3003/api2/image_tagging/", body)
        console.log(res)
        setTimeout(function(){ }, 10000)
        this.setState({tagging: false})
        if(this.state.targetUrl){
          this.setState({getTagsButton: false})
        }
      }
    }

    this.getTags = async () => {
      if(this.state.targetUrl != ""){
        this.setState({targetTags: {}, color: []})
        const body = { "IMG_URL": this.state.targetUrl }
        const res = await api.post("http://206.189.166.235:3003/api2/get_tag/", body)
        this.setState({ targetTags: res[0].IMG_TAGS })
        console.log(this.state.targetTags)
        this.setState(prevState => ({
          targetTags: JSON.parse(prevState.targetTags.replace(/'/g, '"'))
        }))
        console.log(this.state.targetTags)
        this.setState({color: this.state.targetTags.color.split(", ")})
        this.setState({colorName: this.state.targetTags.color_names.split(",")})
        console.log(this.state.color)
        console.log(this.state.colorName)
      }
      
    }
  }

  handleSubmit(e){
    e.preventDefault();
    this.tagIt()
  }

  handleGetTags(e){
    e.preventDefault();
    this.getTags()
  }

  handleChange(e){
    this.setState({[e.target.name]: e.target.value})
    
  }

  handleUploadImage(ev) {
    ev.preventDefault();

    const data = new FormData();
    data.append('file', this.uploadInput.files[0]);
    data.append('filename', this.fileName.value);
    console.dir(data)
    fetch('http://206.189.166.235:3003/api2/upload', {
      method: 'POST',
      body: data,
    }).then((response) => {
      response.json().then((body) => {
        this.setState({ fileUrl: `http://localhost:8000/${body.file}` });
      });
    });
  }

  handleSelect(e){
    this.setState({targetImg: e.target.value})

  }

  handleDeleteOne(){
    this.state.img[this.state.targetImg].tags.pop()
    this.forceUpdate(()=>this.state.tags)
  }

  handleDeleteAll(){
    let target = this.state.targetImg
    let img = this.state.img
    this.setState(img[target].tags = [])
    this.forceUpdate(()=>this.state.tags)
  }
  
  render() {
    
    return (   
        <div className="container-fluid mx-3 my-3">
          <div className="animated fadeIn">
            <div className="row mb-4">
              <div className="col-sm-12">
                <h2>Product Tagging</h2>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 mb-5">
                <label htmlFor="urlImage"><strong>Send Image URL</strong></label>
                <div className="form-group py-2 form-inline">
                  <input type="text" size="35" name="targetUrl" id="urlImage" onChange={this.handleChange} className="mr-4 form-control"/>
                  <div className="mt-2 mt-sm-0">
                    <button onClick={this.handleSubmit} className="btn btn-primary font-weight-bold">TAG IT</button>
                    <button onClick={this.handleGetTags} disabled={this.state.getTagsButton} className="btn btn-primary ml-4 font-weight-bold">GET TAGS</button>
                  </div>
                </div>
                <hr></hr>
                <label htmlFor="urlImage"><strong>Send Image/s FILE (rar, zip, png, jpg, gif)</strong></label>
                <label htmlFor='urlImage'>
                    <i className="cui-file h4 ml-2"></i>
                </label>
                <form className="upload-div col-12 col-sm-6" onSubmit={this.handleUploadImage}>
                  <div className="my-2">
                    <input id="upload" className="upload mx-auto" ref={(ref) => { this.uploadInput = ref; }} type="file" />
                    <label htmlFor="upload"><i className="fas fa-upload mr-3"></i><span className="d-none d-sm-block">choose your file</span></label>
                  </div>
                  <div className="my-2">
                    <input className="form-control" ref={(ref) => { this.fileName = ref; }} type="text" placeholder="Enter the desired name of file" />
                  </div>
                  <br />
                  <div className="mb-2">
                    <button className="btn btn-primary form-control">Upload</button>
                  </div>
                </form>
                {this.state.tagging && 
                <div className="col-6 alert alert-warning" role="alert">
                  We're tagging your image, it will take a few seconds...
                  <i className="ml-2 fa fa-refresh fa-spin"></i>
                </div>}
              </div>
            </div>
            {/* <h3 className="mx-auto mb-4">{this.state.img[this.state.targetImg].name}</h3> */}
            <div className="card">
              <div className="card-body">
                <div className="row">
            
              <div className="px-3 py-3 col-sm-5">
                {this.state.targetUrl ? <img className="img-fluid" src={this.state.targetUrl}/> : <img className="img-fluid" src="img/IR.png"/>}
              </div>
              <div className="col-sm-7">
                <table className="table table-responsive">
                  <thead>
                    <tr>
                      <th className="text-muted width20 text-uppercase">Attribute</th>
                      <th className="text-muted width60 text-uppercase">Value</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td className="text-mutedx2">
                        COLOR:
                      </td>
                      <td>
                      {this.state.color.length > 0 && this.state.color.map((value, index) => {
                        return (
                          <div className="input-group mb-3">
                            <div  className=" input-group-prepend">
                              <label style={{backgroundColor: value}} className="input-group-text" htmlFor="inputGroupSelect01"></label>
                            </div>
                              <input readOnly value={this.state.colorName[index]} className="col-12 color-text form-control" id="inputGroupSelect01"/>
                            
                              
                          </div>
                        )
                      })}
                        
                      </td>
                    </tr>
                    <tr>
                      <td className="text-mutedx2">
                        CATEGORIES:
                      </td>
                      <td>
                        {this.state.targetTags.category && <input readOnly value={this.state.targetTags.category} className="col-12 form-control"/>}
                      </td>
                    </tr>
                    <tr>
                      <td className="text-mutedx2">
                        GENDER:
                      </td>
                      <td>
                      {this.state.targetTags.gender && <input readOnly value={this.state.targetTags.gender} className="col-12 form-control"/>}
                      
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              
              
            
            </div>
            </div>
            </div>
            <div className="row">
              
            </div>
          </div>
        </div>
    )
  }
}

export default MainDetails;