import axios from 'axios';
import { loginMock } from './mocks';

const USER_TOKEN = localStorage.getItem("accessToken")
const AuthStr = 'Bearer ' + USER_TOKEN;

const post = async (url = '', params = {}) => {

  if(params.mockMode) {
    return await new Promise( resolve => {
      setTimeout(() => {
        if(params.user === loginMock.user && params.password === loginMock.password) resolve(true);
        
        resolve(false);
      },2000);
    })
  }

  return await new Promise( (resolve, reject) => {
    axios.post(url, params, { 'headers': { 'Authorization': AuthStr, 'X-XSS-Protection': 0 } })
      .then(res => {
        resolve(res.data);
      }).catch(error => {
        reject(new Error(error));
      });
  });
}

export default post;