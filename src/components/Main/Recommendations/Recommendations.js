import React, { Component } from 'react'
import { Line } from 'react-chartjs-2';
import {
  bussinesChart,
  lineChart
} from '../../../data'

class Recommendations extends Component {
  state = {
    lineChart,
    bussinesChart,
  }

  render() {
    return (
      <React.Fragment>
        <div className="row mb-4">
          <div className="col-sm-12"><a name="recomendations">
              <h2 id="recommendations">Recommendations</h2>
            </a></div>
        </div>
        <div className="row mb-4">
          <div className="col-sm-12">
            <h3>Products Category</h3>
          </div>
        </div>
        <div className="row mb-4">
          <div className="col-sm-6"><strong>Filtrar por:</strong><br/><br/>
            <select className="form-control" name="filtrarPorOption">
              <option value="1">Top Category</option>
              <option value="2">Top Item</option>
              <option value="3">Top Color</option>
              <option value="4">Top Fit</option>
              <option value="5">Top Style</option>
              <option value="6">Top Size</option>
            </select>
          </div>
          <div className="col-sm-3">
            <div className="float-right"><strong>Seleccionar fecha:</strong><br/><br/>
              <input className="form-control" type="date" name="fechaInput"></input>
            </div>
          </div>
          <div className="col-sm-3">
            <div className="btn-group btn-group-toggle float-right mt-5" data-toggle="buttons">
              <label className="btn btn-outline-secondary">
                <input id="option1" type="radio" name="options" autoComplete="off"></input> Day
              </label>
              <label className="btn btn-outline-secondary active">
                <input id="option2" type="radio" name="options" autoComplete="off" ></input> Month
              </label>
              <label className="btn btn-outline-secondary">
                <input id="option3" type="radio" name="options" autoComplete="off"></input> Year
              </label>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-12">
            <div className="card">
              <div className="card-header"><i className="fa fa-align-justify"></i> Results</div>
              <div className="card-body">
                <table className="table table-dark table-responsive-sm">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Img</th>
                      <th>Category</th>
                      <th>Color</th>
                      <th>Bodyshape</th>
                      <th>Ocassion</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>imagen1</td>
                      <td>cardigan</td>
                      <td><span className="badge badge-success p-2"></span></td>
                      <td>apple</td>
                      <td>freetime</td>
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>imagen2</td>
                      <td>trousers</td>
                      <td><span className="badge badge-danger p-2"></span></td>
                      <td>neat hourglass</td>
                      <td>work</td>
                    </tr>
                    <tr>
                      <td>3</td>
                      <td>imagen3</td>
                      <td>skirts</td>
                      <td><span className="badge badge-secondary p-2"></span></td>
                      <td>column</td>
                      <td>work</td>
                    </tr>
                    <tr>
                      <td>4</td>
                      <td>imagen4</td>
                      <td>shirt</td>
                      <td><span className="badge badge-warning p-2"></span></td>
                      <td>rectangle</td>
                      <td>night out</td>
                    </tr>
                    <tr>
                      <td>5</td>
                      <td>imagen5</td>
                      <td>leggins</td>
                      <td><span className="badge badge-success p-2"></span></td>
                      <td>column</td>
                      <td>work out</td>
                    </tr>
                  </tbody>
                </table>
                <ul className="pagination">
                  <li className="page-item"><a className="page-link" href="#">Prev</a></li>
                  <li className="page-item active"><a className="page-link" href="#">1</a></li>
                  <li className="page-item"><a className="page-link" href="#">2</a></li>
                  <li className="page-item"><a className="page-link" href="#">3</a></li>
                  <li className="page-item"><a className="page-link" href="#">4</a></li>
                  <li className="page-item"><a className="page-link" href="#">Next</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-12">
            <div className="card">
              <div className="card-header">Recommendations x Actions
                <div className="card-header-actions"><a className="card-header-action" href="http://www.chartjs.org" target="_blank" rel="noopener noreferrer"></a><small className="text-muted">docs</small></div>
                <div className="card-body">
                  <div className="row">
                    <div className="col-sm-4"><strong>Select KPI:</strong>
                      <select className="form-control" name="selectKPIOption" placeholder="Select KPI">
                        <option value="1">AOV</option>
                        <option value="2">AIV</option>
                        <option value="3">Cross Selling</option>
                        <option value="4">Completed Survey</option>
                        <option value="5">...</option>
                      </select>
                    </div>
                    <div className="col-sm-4"><strong>Customer Characteristics:</strong>
                      <select className="form-control" name="customCaracteristicsOption">
                        <option value="1">All</option>
                        <option value="2">Bodyshape</option>
                        <option value="3">Eyes color</option>
                        <option value="4">Location</option>
                        <option value="5">Ocupation</option>
                        <option value="6">...</option>
                      </select>
                    </div>
                    <div className="col-sm-4"><strong>Loyal Customers:</strong>
                      <select className="form-control" name="loyalCustomOption">
                        <option value="1">Type of Customer</option>
                        <option value="2">New</option>
                        <option value="3">Recurrent</option>
                        <option value="4">Winback</option>
                        <option value="5">Loyal</option>
                        <option value="6">...</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div className="chart-wrapper">
                  <Line data={lineChart.data}
                        options={lineChart.options}
                        id="canvas-1" />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-12">
            <h3>Business conditions</h3>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12">
            <div className="card">
              <div className="card-header"><i className="fa fa-align-justify"></i> Combined All Table</div>
              <div className="card-body">
                <table className="table table-dark table-responsive-sm table-bordered table-striped table-sm">
                  <thead>
                    <tr>
                      <th>Username</th>
                      <th>Date registered</th>
                      <th>Role</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Vishnu Serghei</td>
                      <td>2012/01/01</td>
                      <td>Member</td>
                      <td><span className="badge badge-success">Active</span></td>
                    </tr>
                    <tr>
                      <td>Zbyněk Phoibos</td>
                      <td>2012/02/01</td>
                      <td>Staff</td>
                      <td><span className="badge badge-danger">Banned</span></td>
                    </tr>
                    <tr>
                      <td>Einar Randall</td>
                      <td>2012/02/01</td>
                      <td>Admin</td>
                      <td><span className="badge badge-secondary">Inactive</span></td>
                    </tr>
                    <tr>
                      <td>Félix Troels</td>
                      <td>2012/03/01</td>
                      <td>Member</td>
                      <td><span className="badge badge-warning">Pending</span></td>
                    </tr>
                    <tr>
                      <td>Aulus Agmundr</td>
                      <td>2012/01/21</td>
                      <td>Staff</td>
                      <td><span className="badge badge-success">Active</span></td>
                    </tr>
                  </tbody>
                </table>
                <nav>
                  <ul className="pagination">
                    <li className="page-item"><a className="page-link" href="#">Prev</a></li>
                    <li className="page-item active"><a className="page-link" href="#">1</a></li>
                    <li className="page-item"><a className="page-link" href="#">2</a></li>
                    <li className="page-item"><a className="page-link" href="#">3</a></li>
                    <li className="page-item"><a className="page-link" href="#">4</a></li>
                    <li className="page-item"><a className="page-link" href="#">Next</a></li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12">
            <div className="card">
              <div className="card-body">
                <div className="row">
                  <div className="col-sm-5">
                    <h4 className="card-title mb-0">Traffic</h4>
                    <div className="small text-muted">November 2017</div>
                  </div>
                  {/* <!-- /.col--> */}
                  <div className="col-sm-7 d-none d-md-block">
                    <button className="btn btn-primary float-right" type="button"><i className="icon-cloud-download"></i></button>
                    <div className="btn-group btn-group-toggle float-right mr-3" data-toggle="buttons">
                      <label className="btn btn-outline-secondary">
                        <input id="option1" type="radio" name="options" autoComplete="off"></input> Day
                      </label>
                      <label className="btn btn-outline-secondary active">
                        <input id="option2" type="radio" name="options" autoComplete="off" ></input> Month
                      </label>
                      <label className="btn btn-outline-secondary">
                        <input id="option3" type="radio" name="options" autoComplete="off"></input> Year
                      </label>
                    </div>
                  </div>
                  {/* <!-- /.col--> */}
                </div>
                {/* <!-- /.row--> */}
                <div className="chart-wrapper" style={{height:"300px", marginTop:"40px"}}>
                  <Line data={bussinesChart.data}
                        options={bussinesChart.options}
                        legend={bussinesChart.options.legend}
                        className ='chart'
                        height={300}
                        id="business-chart" />
                </div>
              </div>
              <div className="card-footer">
                <div className="row text-center">
                  <div className="col-sm-12 col-md mb-sm-2 mb-0">
                    <div className="text-muted">Visits</div><strong>29.703 Users (40%)</strong>
                    <div className="progress progress-xs mt-2">
                      <div className="progress-bar bg-success" role="progressbar" style={{width: "40%"}} aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div>
                  <div className="col-sm-12 col-md mb-sm-2 mb-0">
                    <div className="text-muted">Unique</div><strong>24.093 Users (20%)</strong>
                    <div className="progress progress-xs mt-2">
                      <div className="progress-bar bg-info" role="progressbar" style={{width: "20%"}} aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div>
                  <div className="col-sm-12 col-md mb-sm-2 mb-0">
                    <div className="text-muted">Likes/Dislikes</div><strong>78.706 Views (60%)</strong>
                    <div className="progress progress-xs mt-2">
                      <div className="progress-bar bg-warning" role="progressbar" style={{width: "60%"}} aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div>
                  <div className="col-sm-12 col-md mb-sm-2 mb-0">
                    <div className="text-muted">New Users</div><strong>22.123 Users (80%)</strong>
                    <div className="progress progress-xs mt-2">
                      <div className="progress-bar bg-danger" role="progressbar" style={{width: "80%"}} aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div>
                  <div className="col-sm-12 col-md mb-sm-2 mb-0">
                    <div className="text-muted">Bounce Rate</div><strong>40.15%</strong>
                    <div className="progress progress-xs mt-2">
                      <div className="progress-bar" role="progressbar" style={{width: "40%"}} aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

export default Recommendations
