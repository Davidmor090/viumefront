import $ from 'jquery'
import {
  setCookie
} from './cookies'

const names = ['silvia', 'david', 'jordi', 'victor']

const login = (j) => {
  j('.btn-login').on('click', () => {
    const username = j('input[type=text]').val()
    const password = j('input[type=password]').val()
    if (names.includes(username) && password === 'viume') {
      setCookie('viume-login', true)
      location.href = '/index.html'
    } else {
      j('.alert').removeClass('d-none')
    }
  })

  j('input[type=text],input[type=password]').on('keyup', () => {
    if (!j('.alert').hasClass('d-none')) {
      j('.alert').addClass('d-none')
    }
  })
}

login($)
