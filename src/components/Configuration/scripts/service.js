function service(domain = '', path = '', params = {}) {
  this.post = () => {

    const headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    };

    const body = JSON.stringify(params);

    const content = {
      headers,
      method: 'POST',
      body
    }

    return new Promise((resolve, reject) => {
      fetch(domain + path, content)
        .then((res) => resolve(res) )
        .catch((error) => reject(error));
    });
  }

  //TODO get, put and other stuff...

  return this;
}

export default service;