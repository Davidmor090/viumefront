import React from 'react'
import PropTypes from 'prop-types';

function ProductTag(props) {
  

  return (
      <button onClick={props.handleClick} type="button" className="shadow mx-2 my-2 btn btn-secondary">
          {props.name}
          <span className="cui-circle-x ml-2"></span>
      </button>
  )
  
}

ProductTag.propTypes = {name: PropTypes.string, key: PropTypes.string, handleClick: PropTypes.func}

export default ProductTag;
