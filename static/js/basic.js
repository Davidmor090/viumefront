var isLogged = function isLogged() {
  if (!getCookie('viume-login')) {
    location.href = '/login.html';
  }
};

isLogged();
//# sourceMappingURL=basic.js.map