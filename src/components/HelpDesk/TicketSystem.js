import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import $ from 'jquery';
import Aside from '../Aside/Aside';
import Footer from '../Footer/Footer';
import Header from '../Header/Header';
import Sidebar from '../Sidebar/Sidebar';
import api from '../../services'

export default class TicketSystem extends Component {
  
  constructor(props) {
    super(props)
    
    this.state = {
      loading: false,
      selected: false,
      ticketCategory: "",
      ticketTextContent: "",
      userEmail: "",
      tickets: []
    }

    this.getAllTickets = async () => {
      const res = await api.get("http://206.189.166.235:3003/api2/get_all_tickets/")
      console.log(res)
      this.setState({tickets: res})
    }

    this.sendSupportMail = async () => {
      const body = { "category": this.state.ticketCategory, "content": this.state.ticketTextContent }
      const res = await api.post("http://127.0.0.1:5000/send", body)
    }

    this.newTicket = async () => {
      this.setState({loading: true})
      const body = {
        "TICKET_ID": this.state.tickets.length+1 ,"GIVEN_EMAIL": this.state.userEmail, "STATUS":"pending", "CATEGORY": this.state.ticketCategory, "CONTENT": this.state.ticketTextContent
      }
      const res = await api.post("http://206.189.166.235:3003/api2/new_ticket/", body)
      console.log(res)
      this.setState({loading: false})
    }

    this.showTicket = (e) => {
      if (e.target.parentNode.children[1].classList.contains("d-none")) {
        e.target.parentNode.children[1].setAttribute("class","d-block")
      }
      else {
        e.target.parentNode.children[1].setAttribute("class","d-none");
      }
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidMount() {
    this.getAllTickets();
  }
  
  handleChange = e => {
    if (e.target.name === "ticketCategory") {
      if (e.target.value === "") {
        this.setState({selected: false});
      }
      else {
        this.setState({[e.target.name]: e.target.value, selected: true});
      }
    }
    else {
      this.setState({[e.target.name]: e.target.value});
    }
    
  }

  handleSubmit = () => {
    if (!this.state.userEmail.includes("@") || this.state.userEmail === "") {
      // const inputEmail = document.querySelector("#issue-email");
      // const prevClasses = inputEmail.getAttribute("class");
      // const newClasses = prevClasses+" is-invalid";
      // inputEmail.setAttribute("class", newClasses)
      $("#issue-email").addClass("is-invalid")
      return alert("you must provide a valid email address")
    }
    $("#issue-email").removeClass("is-invalid")
    this.sendSupportMail();
    this.newTicket();
    this.getAllTickets();
    this.render();
  }

  render() {
    return (
      <div className="app header-fixed sidebar-expanded aside-menu-fixed sidebar-lg-show">
      <Header></Header>
        <div className="app-body">
          <Sidebar></Sidebar>
          <div className="container">
            <div className="row">
              <div className="col-8 mx-auto text-center my-2">
                <h1 className="my-4">Contact Support</h1>
              </div>
              <div className="card my-2 shadow col-12">
                <div className="my-5 row">
                  <div className="ticket-card-content mx-auto col-6">
                    <p className="mb-5">
                      Need assistance? Submit a request below and we will get to work!
                      If you think the problem is a bug and you dont find it in <span><Link to="/faqs">FAQs</Link></span> or <span><Link to="/documentation">Documentation</Link></span>, 
                      raise it here and we will collect full details to report it to development.
                    </p>
                    <div className="form-group">
                      <label className="text-muted font-weight-bold" htmlFor="issue-categories">What can we help you with?</label>
                      <select onChange={this.handleChange} className="form-control" name="ticketCategory" id="issue-categories">
                        <option value=""></option>
                        <option value="Recommendation problems">Recommendation problems</option>
                        <option value="Interface problems">Interface problems</option>
                        <option value="Wrong image recognition">Wrong image recognition</option>
                        <option value="Others">Others</option>
                      </select>
                    </div>
                    
                    {this.state.selected && ( <div className="mt-5 ticket-submission form-group">
                      <label className="text-muted font-weight-bold" htmlFor="issue-email">Your Email address</label>
                      <input type="email" onChange={this.handleChange} value={this.state.userEmail} className="mb-4 form-control" name="userEmail" id="issue-email"/>
                      <label className="text-muted font-weight-bold" htmlFor="ticket-textarea">Give us the details</label>
                      <textarea onChange={this.handleChange} name="ticketTextContent" className="form-control" id="ticket-textarea" rows="3"></textarea>
                      <button type="submit" onClick={this.handleSubmit} className="btn btn-primary my-3">Submit support request</button>
                    </div> )}
                  </div>
                </div>
              </div>
              {this.state.tickets.length > 0 && (<div className="card my-2 shadow col-12">
                <div className="my-5 row">
                  <div className="col-8 p-4 shadow border mx-auto">
                    <h3 className="mb-4">My Tickets</h3>
                    {this.state.tickets.map( (value, index) => {
                      return (<div key={index} className={`col-10 ticket p-2 mt-2 shadow border ${value.STATUS === "pending" && "bg-secondary"} ${value.STATUS === "in progress" && "bg-warning"} ${value.STATUS === "solved" && "bg-success"}`}>
                                <h4 onClick={this.showTicket} className="py-4 ticket-header">Ticket ID: {value.TICKET_ID}</h4>
                                <div className="d-none">
                                  <ul>
                                    <li><span className="font-weight-bold">status:</span><span class="ml-2 p-1 shadow badge badge-info">{value.STATUS}</span></li>
                                    <li><span className="font-weight-bold">category:</span> {value.CATEGORY}</li>
                                    <li><span className="font-weight-bold">content:</span> {value.CONTENT}</li>
                                  </ul>
                                </div>
                              </div>)
                    })}
                  </div>
                </div>
              </div>
              )}
            </div>
          </div>
          <Aside></Aside>
        </div>
        <Footer></Footer>
      </div>
    )
  }
}