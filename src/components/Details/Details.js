import React, { Component } from 'react'

import Aside from '../Aside/Aside';
import Footer from '../Footer/Footer';
import Header from '../Header/Header';
import Sidebar from '../Sidebar/Sidebar';
import MainDetails from './MainDetails';



class Details extends Component {
  render() {
    return ( 
        <div className="app header-fixed sidebar-expanded aside-menu-fixed sidebar-lg-show">
        <Header></Header>
          <div className="app-body">
            <Sidebar></Sidebar>
            <MainDetails></MainDetails>
            <Aside></Aside>
          </div>
        <Footer></Footer>
        </div>
    )
  }
}

export default Details;