import React from 'react';
import PropTypes from 'prop-types';

export default function UploadImage (props) {
  
  return (<div className='buttons fadein'>
    <div className='button'>
      <label htmlFor='single'>
        <i className="cui-file h4 mr-2"></i>
      </label>
      <input type='file' id='single' onChange={props.handleUpload} /> 
    </div>
    
    <div className='button'>
      <label htmlFor='multi'>
        <i className="cui-file h4 mr-2"></i>
      </label>
      <input type='file' id='multi' onChange={props.handleUpload} multiple />
    </div>
  </div>)
}

UploadImage.propTypes = {handleUpload: PropTypes.func}
