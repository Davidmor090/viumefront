import React, { Component } from 'react';
import $ from 'jquery';
import Aside from '../Aside/Aside';
import Footer from '../Footer/Footer';
import Header from '../Header/Header';
import Sidebar from '../Sidebar/Sidebar';

export default class FAQs extends Component {

  constructor(props){
    super(props)



    this.showRecomProblems = function(){
      
      if($("#showRecomProblems").hasClass("cui-chevron-top")){
        $("#showRecomProblems").removeClass("cui-chevron-top")
        $("#showRecomProblems").addClass("cui-chevron-bottom")
      }
      else if($("#showRecomProblems").hasClass("cui-chevron-bottom")){
        $("#showRecomProblems").removeClass("cui-chevron-bottom")
        $("#showRecomProblems").addClass("cui-chevron-top")
      }

      if($(".recomproblemsdisplay").hasClass("d-none")){
        $(".recomproblemsdisplay").removeClass("d-none")
        $(".recomproblemsdisplay").addClass("d-block")
      }
      else if($(".recomproblemsdisplay").hasClass("d-block")){
        $(".recomproblemsdisplay").removeClass("d-block")
        $(".recomproblemsdisplay").addClass("d-none")
      }
        
    }

    this.showUiProblems = function(){
      
      if($("#showUiProblems").hasClass("cui-chevron-top")){
        $("#showUiProblems").removeClass("cui-chevron-top")
        $("#showUiProblems").addClass("cui-chevron-bottom")
      }
      else if($("#showUiProblems").hasClass("cui-chevron-bottom")){
        $("#showUiProblems").removeClass("cui-chevron-bottom")
        $("#showUiProblems").addClass("cui-chevron-top")
      }

      if($(".uiproblemsdisplay").hasClass("d-none")){
        $(".uiproblemsdisplay").removeClass("d-none")
        $(".uiproblemsdisplay").addClass("d-block")
      }
      else if($(".uiproblemsdisplay").hasClass("d-block")){
        $(".uiproblemsdisplay").removeClass("d-block")
        $(".uiproblemsdisplay").addClass("d-none")
      }
        
    }

    this.showIrProblems = function(){
      
      if($("#showIrProblems").hasClass("cui-chevron-top")){
        $("#showIrProblems").removeClass("cui-chevron-top")
        $("#showIrProblems").addClass("cui-chevron-bottom")
      }
      else if($("#showIrProblems").hasClass("cui-chevron-bottom")){
        $("#showIrProblems").removeClass("cui-chevron-bottom")
        $("#showIrProblems").addClass("cui-chevron-top")
      }

      if($(".irproblemsdisplay").hasClass("d-none")){
        $(".irproblemsdisplay").removeClass("d-none")
        $(".irproblemsdisplay").addClass("d-block")
      }
      else if($(".irproblemsdisplay").hasClass("d-block")){
        $(".irproblemsdisplay").removeClass("d-block")
        $(".irproblemsdisplay").addClass("d-none")
      }
        
    }

    this.showOtherProblems = function(){
      
      if($("#showOtherProblems").hasClass("cui-chevron-top")){
        $("#showOtherProblems").removeClass("cui-chevron-top")
        $("#showOtherProblems").addClass("cui-chevron-bottom")
      }
      else if($("#showOtherProblems").hasClass("cui-chevron-bottom")){
        $("#showOtherProblems").removeClass("cui-chevron-bottom")
        $("#showOtherProblems").addClass("cui-chevron-top")
      }

      if($(".otherproblemsdisplay").hasClass("d-none")){
        $(".otherproblemsdisplay").removeClass("d-none")
        $(".otherproblemsdisplay").addClass("d-block")
      }
      else if($(".otherproblemsdisplay").hasClass("d-block")){
        $(".otherproblemsdisplay").removeClass("d-block")
        $(".otherproblemsdisplay").addClass("d-none")
      }
        
    }

    
  }



  render() {
    return (
      <div className="app header-fixed sidebar-expanded aside-menu-fixed sidebar-lg-show">
      <Header></Header>
        <div className="app-body">
          <Sidebar></Sidebar>
          <div className="container">
            <div className="row">
              <div className="col-8 mx-auto text-center my-2">
                <h1 className="my-4">FAQs</h1>
              </div>
              <div id="recomproblems" className="card my-2 shadow col-12">
                <div className="card-body">
                  <h1><span title="show/hide FAQs" onClick={this.showRecomProblems} id="showRecomProblems" className="iconToggler icons rounded text-white py-2 px-2 font-3xl mr-4 mt-5 cui-chevron-bottom"></span>Recommendation Problems</h1>
                  <div className="recomproblemsdisplay mt-5 d-block">
                    <div className="QA-pair">
                      <div className="Question">
                        What is Lorem Ipsum?
                      </div>
                      <div className="Answer">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industrys standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                        It has survived not only five centuries, but also the leap into electronic typesetting,
                        remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                        and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                      </div>
                    </div>
                    <div className="QA-pair">
                      <div className="Question">
                        What is Lorem Ipsum?
                      </div>
                      <div className="Answer">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industrys standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                        It has survived not only five centuries, but also the leap into electronic typesetting,
                        remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                        and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div id="uiproblems" className="card my-2 shadow col-12">
                <div className="card-body">
                  <h1><span title="show/hide FAQs" onClick={this.showUiProblems} id="showUiProblems" className="iconToggler icons rounded text-white py-2 px-2 font-3xl mr-4 mt-5 cui-chevron-bottom"></span>Interface Problems</h1>
                  <div className="uiproblemsdisplay mt-5 d-block">
                  <div className="QA-pair">
                      <div className="Question">
                        What is Lorem Ipsum?
                      </div>
                      <div className="Answer">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industrys standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                        It has survived not only five centuries, but also the leap into electronic typesetting,
                        remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                        and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                      </div>
                    </div>
                    <div className="QA-pair">
                      <div className="Question">
                        What is Lorem Ipsum?
                      </div>
                      <div className="Answer">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industrys standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                        It has survived not only five centuries, but also the leap into electronic typesetting,
                        remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                        and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div id="irproblems" className="card my-2 shadow col-12">
                <div className="card-body">
                  <h1><span title="show/hide FAQs" onClick={this.showIrProblems} id="showIrProblems" className="iconToggler icons rounded text-white py-2 px-2 font-3xl mr-4 mt-5 cui-chevron-bottom"></span>Wrong Image Recognition</h1>
                  <div className="irproblemsdisplay mt-5 d-block">
                    <div className="QA-pair">
                      <div className="Question">
                        What is Lorem Ipsum?
                      </div>
                      <div className="Answer">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industrys standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                        It has survived not only five centuries, but also the leap into electronic typesetting,
                        remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                        and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                      </div>
                    </div>
                    <div className="QA-pair">
                      <div className="Question">
                        What is Lorem Ipsum?
                      </div>
                      <div className="Answer">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industrys standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                        It has survived not only five centuries, but also the leap into electronic typesetting,
                        remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                        and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div id="otherproblems" className="card my-2 shadow col-12">
                <div className="card-body">
                  <h1><span title="show/hide FAQs" onClick={this.showOtherProblems} id="showOtherProblems" className="iconToggler icons rounded text-white py-2 px-2 font-3xl mr-4 mt-5 cui-chevron-bottom"></span>Others</h1>
                  <div className="otherproblemsdisplay mt-5 d-block">
                    <div className="QA-pair">
                      <div className="Question">
                        What is Lorem Ipsum?
                      </div>
                      <div className="Answer">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industrys standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                        It has survived not only five centuries, but also the leap into electronic typesetting,
                        remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                        and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                      </div>
                    </div>
                    <div className="QA-pair">
                      <div className="Question">
                        What is Lorem Ipsum?
                      </div>
                      <div className="Answer">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industrys standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                        It has survived not only five centuries, but also the leap into electronic typesetting,
                        remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                        and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Aside></Aside>
        </div>
        <Footer></Footer>
      </div>
    )
  }
}