import React, { Component } from 'react';
import { HashLink } from 'react-router-hash-link';
import { Link } from 'react-router-dom';


class Sidebar extends Component {
  render() {
    return (
        <div className="sidebar">
          <nav role="navigation" className="sidebar-nav">
            <ul className="nav nav-content">
              <li className="nav-title">Viume</li>
              <li className="nav-item nav-dropdown"><a className="nav-link nav-dropdown-toggle" href="#"><i className="nav-icon icon-speedometer"></i> Dashboard</a>
                <ul className="nav-dropdown-items">
                  <HashLink className="nav-link" to={'/#overview'}><li className="nav-item"><i className="nav-icon icons cui-pie-chart"></i> Overview</li></HashLink>
                  <HashLink className="nav-link" to={'/#recommendations'}><li className="nav-item"><i className="nav-icon icons cui-star"></i> Recommendations</li></HashLink>
                  <HashLink className="nav-link" to={'/#customers'}><li className="nav-item"><i className="nav-icon icons cui-people"></i> Customers</li></HashLink>
                </ul>
              </li>
              <li className="nav-item nav-dropdown"><a className="nav-link nav-dropdown-toggle" href="#"><i className="nav-icon icons cui-cog"></i>REC Configuration</a>
                <ul className="nav-dropdown-items">
                  <HashLink className="nav-link" to={'/custom-recommender#generalrules'}><li className="nav-item"><i className="nav-icon icons cui-cog"></i> General Rules</li></HashLink>
                  <HashLink className="nav-link" to={'/custom-recommender#outfitrules'}><li className="nav-item"><i className="nav-icon icons cui-cog"></i> Outfit Rules</li></HashLink>
                  <HashLink className="nav-link" to={'/custom-recommender#customerrules'}><li className="nav-item"><i className="nav-icon icons cui-user"></i> Customer Rules</li></HashLink>
                  <HashLink className="nav-link" to={'/custom-recommender#savedrules'}><li className="nav-item"><i className="nav-icon icons cui-briefcase"></i> Saved Rules</li></HashLink>
                </ul>
              </li>
              {/* <li className="nav-item"><a className="nav-link" href="configuration.html"><i className="nav-icon icons cui-cog"></i> Configuration</a></li> */}
              {/* <li className="nav-item"><a className="nav-link" href="accounts.html"><i className="nav-icon icon-user"></i> Account</a></li> */}
              <Link className="nav-link" to={'/details'}><li className="nav-item"><i className="nav-icon icons cui-tags"></i>Product Tagging</li></Link>
            </ul>
          </nav>
          <button className="sidebar-minimizer" type="button"></button>
        </div>
    )
  }
}

export default Sidebar
