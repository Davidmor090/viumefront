const express = require('express');
const bodyParser = require('body-parser');
var cors = require('cors');

const app = express();
const port = process.env.PORT || 5000;

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));




const mailjet = require ('node-mailjet')
.connect('d1f1b6d5eb4f0c6be108bd60d5a8f488', 'ff7f8a0af078e37c27115e20fccbe57a')



app.post('/send', (req, res) => {
  const {category, content} = req.body;  
  res.send(
    sendMail(category, content)
    );
});


sendMail = (category, content) => {
  const request = mailjet
  .post("send", {'version': 'v3.1'})
  .request({
    "Messages":[
      {
        "From": {
          "Email": "david.mor@viume.co",
          "Name": "David"
        },
        "To": [
          {
            "Email": "david.mor@viume.co",
            "Name": "David"
          }
        ],
        "Subject": "Greetings from Mailjet.",
        "TextPart": "My first Mailjet email",
        "HTMLPart": `<h3>${category}</h3><br />${content}`,
        "CustomID": "AppGettingStartedTest"
      }
    ]
  })
  request
    .then((result) => {
      console.log(result.body)
      sendConfirmMail()
    })
    .catch((err) => {
      console.log(err.statusCode)
    })
}

sendConfirmMail = () => {
  const request = mailjet
  .post("send", {'version': 'v3.1'})
  .request({
    "Messages":[
      {
        "From": {
          "Email": "david.mor@viume.co",
          "Name": "David"
        },
        "To": [
          {
            "Email": "dmlaporta1990@gmail.com",
            "Name": "David"
          }
        ],
        "Subject": "Greetings from Viume",
        "TextPart": "My first Mailjet email",
        "HTMLPart": `<h3>Viume tech team</h3><br />Your ticket has ben submitted successfully.<br />In short you will recieve a response from our tech team`,
        "CustomID": "AppGettingStartedTest"
      }
    ]
  })
  request
    .then((result) => {
      console.log(result.body)
    })
    .catch((err) => {
      console.log(err.statusCode)
    })
}




app.listen(port, () => console.log(`Listening on port ${port}`));
