import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Aside from '../Aside/Aside';
import Footer from '../Footer/Footer';
import Header from '../Header/Header';
import Sidebar from '../Sidebar/Sidebar';

export default class HelpDesk extends Component {
  render() {
    return (
      <div className="app header-fixed sidebar-expanded aside-menu-fixed sidebar-lg-show">
        <Header></Header>
          <div className="app-body">
            <Sidebar></Sidebar>
            <div className="container">
              <div className="row">
                <div className="col-8 mx-auto text-center my-2">
                  <h1 className="my-4">Technical Support</h1>
                  <div className="btn-group-vertical d-none d-sm-block">
                    <Link to="/faqs"><button type="button" className="btn btn-helpdesk btn-secondary btn-l">FAQs</button></Link>
                    <Link to="/documentation"><button type="button" className="btn btn-helpdesk btn-secondary btn-l">Documentation</button></Link>
                    <Link to="/contact-support"><button type="button" className="btn btn-helpdesk btn-secondary btn-l">Support</button></Link>
                  </div>
                  <div className="btn-group-vertical d-block d-sm-none">
                    <Link to="/faqs"><button type="button" title="FAQs" className="btn btn-helpdesk btn-secondary btn-l"><i className="icons icon-help-desk fa fa-question-circle"></i></button></Link>
                    <Link to="/documentation"><button type="button" title="Documentation" className="btn btn-helpdesk btn-secondary btn-l"><i className="icons icon-help-desk fa fa-book"></i></button></Link>
                    <Link to="/contact-support"><button type="button" title="Submit a Ticket" className="btn btn-helpdesk btn-secondary btn-l"><i className="icons icon-help-desk fa fa-ticket"></i></button></Link>
                  </div>
                </div>
              </div>
            </div>
            <Aside></Aside>
          </div>
        <Footer></Footer>
      </div>   
    )
  }
}
