import React, { Component } from 'react'

class MarketingActivities extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-sm-12"><a name="marketing">
              <h2>Marketing Activities</h2>
            </a></div>
        </div>
        <div className="row">
          <div className="col-sm-12">
            <div className="list-group"><a className="list-group-item list-group-item-action">Remarketing</a><a className="list-group-item list-group-item-action">Direct marketing</a><a className="list-group-item list-group-item-action">Social ads</a><a className="list-group-item list-group-item-action">Traffic</a></div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

export default MarketingActivities
