import React, { Component } from 'react'

class Footer extends Component {
  render() {
    return (
      <footer style={{position: "fixed", bottom: "0px", width: "100%"}} className="app-footer">
        <div style={{position: "relative", left: "45%"}}><a href="https://viume.com">Viume</a><span>&copy; 2018.</span></div>
      </footer>
    )
  }
}

export default Footer
