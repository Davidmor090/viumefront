import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Aside from '../Aside/Aside';
import Footer from '../Footer/Footer';
import Header from '../Header/Header';
import Sidebar from '../Sidebar/Sidebar';

export default class ContactUs extends Component {
  render() {
    return (
      <div className="app header-fixed sidebar-expanded aside-menu-fixed sidebar-lg-show">
        <Header></Header>
          <div className="app-body">
            <Sidebar></Sidebar>
            <div className="container">
              <div className="row">
                <div className="col-8 mx-auto text-center my-4">
                  <h1 className="my-4">Contact Us</h1>
                  <div className="card">
                    <div className="mx-auto col-8 card-body">
                      <div className="h5 text-muted text-center my-2">Contact Information:</div>
                      <div>
                        <i className="fa fa-envelope align-center mx-2 text-center my-2" aria-hidden="true"></i>
                          Mail : <span className="font-weight-bold">contacto@viume.co</span>
                      </div>
                      <div>
                        <i className="fa fa-phone align-center mx-2 text-center my-2" aria-hidden="true"></i>
                          Phone : <span className="font-weight-bold">626444123</span>
                      </div>
                      <hr className="my-4"/>
                      <div className="my-2">
                        <Link to="/help-desk">
                          <button className="px-2 py-2 btn btn-md btn-success font-weight-bold">Help Desk</button>
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
                
              </div>
            </div>
            <Aside></Aside>
          </div>
        <Footer></Footer>
      </div>
    )
  }
}

