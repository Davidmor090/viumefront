import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import UserContext from '../../UserContext';
import { browserHistory } from 'react-router';

class Header extends Component {
  
  static contextType = UserContext;
  
  constructor(props) {
    super(props)
    const USER_TOKEN = localStorage.getItem("refreshToken")
    const AuthStr = 'Bearer ' + USER_TOKEN;
      
    fetch("http://206.189.166.235:3003/api2/refresh",
      {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': AuthStr
        },
        method: "GET"
        
        
      })
      .then(function(res){ return res.json() })
      .then(function(res){ return localStorage.setItem("accessToken", res.access_token) })
      .catch(function(res){ res })

  }
  
  
  render() {
    return (

      <header style={{display: 'fixed', top: '0px'}} className="app-header navbar">
        <button className="navbar-toggler iconheader sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show"><span className="sidebar-minimizer navbar-toggler-icon"></span></button><a className="navbar-brand" href="#"><img className="navbar-brand-full" src="img/brand/viume-small.png" width="89" height="25" alt="CoreUI Logo"></img><img className="navbar-brand-minimized" src="img/brand/sygnet.svg" width="30" height="30" alt="CoreUI Logo"></img></a>
          <button className="d-none navbar-toggler sidebar-toggler " type="button" data-toggle="sidebar-lg-show"><span className="sidebar-minimizer navbar-toggler-icon"></span></button>
          <ul className="nav navbar-nav ml-auto">
            <Link to="/my-profile">
              <li title="my profile" className="nav-item dropdown d-sm-down-none">
                <img className="img-avatar" height="70%" width="70%" src="img/avatars/6.png" alt="admin@bootstrapmaster.com"></img>
                <div className="dropdown-menu dropdown-menu-right">
                  <div className="dropdown-header text-center">
                    <strong>Account</strong>
                  </div>
                </div>
              </li>
            </Link>
            <li title="contact us" className="nav-item ml-2">
              <Link className="nav-link" to="/contact-us">
                <i className="fa fa-envelope"></i>
                <span className="d-md-down-none ml-md-1">Contact Us</span>
              </Link>
            </li>
            <li title="support" className="nav-item px-md-2 mx-md-2">
              <Link className="nav-link" to="/help-desk">  
                <i className="fa fa-question-circle"></i>
                <span className="d-md-down-none ml-md-1">Support</span>
              </Link>
            </li>
            {localStorage.getItem("accessGranted") && <li title="logout" className="nav-item logout-li px-md-2 mx-md-2">
              <Link className="nav-link" to="/login">  
                <i className="fa fa-sign-out"></i>
                <span className="d-md-down-none ml-md-1">Logout</span>
              </Link>
            </li>}
          </ul>
            <button className="navbar-toggler sidebar-toggler d-none" type="button" data-toggle="sidebar-lg-show">
              <span className="navbar-toggler-icon"></span>
            </button>
      </header>

    )
  }
}
      
export default Header
