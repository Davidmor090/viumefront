import React, { Component } from 'react';
import Aside from '../Aside/Aside';
import Footer from '../Footer/Footer';
import Header from '../Header/Header';
import Sidebar from '../Sidebar/Sidebar';
import ConfigRules from './ConfigRules';

class ConfigRecommender extends Component {
  render() {
    return (
      <div className="app header-fixed sidebar-expanded aside-menu-fixed sidebar-lg-show">
        <Header></Header>
          <div className="bg-viume app-body">
            <Sidebar></Sidebar>
            <ConfigRules />
            <Aside></Aside>
          </div>
        <Footer></Footer>
      </div>
    )
  }
}

export default ConfigRecommender;