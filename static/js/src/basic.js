import {
  getCookie
} from './cookies'

const isLogged = () => {
  if (!getCookie('viume-login')) {
    location.href = '/login.html'
  }
}

isLogged()
