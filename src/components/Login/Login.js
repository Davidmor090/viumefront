import React, { Component } from 'react';
import api from '../../services';
import UserContext from '../../UserContext';
import { Link } from 'react-router-dom';
import * as path from 'path';

class Login extends Component {
  static contextType = UserContext;

  constructor(props) {
    super(props);

    if(localStorage.getItem("accessGranted")) {
      const res = api.get('http://206.189.166.235:3003/api2/logout');
      if(res) {
        localStorage.clear();
      }
    }
    

    this.state = {
      username: '',
      password: '',
      submitted: false,
      loading: false,
      error: false,
      user_id: 0,
      mockMode: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

    // this.getCredentials = function(){
    //   const that = this
    //   let userid = 0
    //   that.setState({loading: true})

    //   fetch("https://206.189.166.235:3003/api2/get_user_credentials/",
    //     {
    //       headers: {
    //         'Accept': 'application/json',
    //         'Content-Type': 'application/json'
    //       },
    //       method: "POST",
    //       body: JSON.stringify({"username":this.state.username, "password": this.state.password})
    //     })
    //     .then(function(res){ return res.json() })
    //     .then(function(res){Object.keys(res).length>0 ? userid = Object.entry(res) : null})
    //     .then(function(){ that.setState({user_id: userid})  })
    //     .then(function(){that.forceUpdate(()=>that.state.user_id)})
    //     .catch(function(res){ res })

    // }
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  handleSubmit = async e => {
    e.preventDefault();

    this.setState({ submitted: true });
    const { username, password } = this.state;

    // this.getCredentials()

    const body = {
      mockMode: this.state.mockMode,
      username: username,
      password: password
    };

    const res = await api.post('http://206.189.166.235:3003/api2/login', body);

    if (!res) {
      this.setState({
        error: true
      });

      return false;
    }
    // res = JSON.parse(res)
    const access_token = res.access_token
    const refresh_token = res.refresh_token
    
    this.setState({ loading: true });
    
    const user = this.context;
    user.accessGranted = true;
    user.accessToken = access_token;
    user.refreshToken = refresh_token;
    
    localStorage.setItem('accessGranted', 'true');
    localStorage.setItem('accessToken', access_token);
    localStorage.setItem('refreshToken', refresh_token);

    this.props.history.push({ pathname: '/' });
    const previousURL = window.location.href.split('/')[0];
    window.location.href = `${previousURL}`;
  };

  showError() {
    if (this.state.error) {
      return (
        <div className='row'>
          <div className='col-12'>
            <div className='alert alert-danger'>
              Incorrect username or password
            </div>
          </div>
        </div>
      );
    }

    return '';
  }

  render() {
    const {
      username,
      password,
      submitted,
      loading,
      error,
      user_id
    } = this.state;

    return (
      <div className='app flex-row align-items-center'>
        <div className='container'>
          <div className='row justify-content-center'>
            <div className='col-md-6'>
              <img
                className='img-fluid'
                src='img/brand/viume-big.png'
                alt='Viume Logo'
              />
            </div>
          </div>
          <div className='row justify-content-center'>
            <div className='col-md-6'>
              <div className='card-group'>
                <div className='card p-4'>
                  <div className='card-body'>
                    <form>
                      <h1>Login</h1>
                      <p className='text-muted'>Sign In to your account</p>
                      <div className='input-group mb-3'>
                        <div className='input-group-prepend'>
                          <span className='input-group-text'>
                            <i className='icon-user' />
                          </span>
                        </div>
                        <input
                          className='form-control'
                          type='text'
                          name='username'
                          value={username}
                          onChange={this.handleChange}
                          placeholder='Username'
                        />
                        {submitted && user_id == {} && (
                          <div className='invalid-feedback'>
                            Username Required
                          </div>
                        )}
                      </div>
                      <div className='input-group mb-4'>
                        <div className='input-group-prepend'>
                          <span className='input-group-text'>
                            <i className='icon-lock' />
                          </span>
                        </div>
                        <input
                          className='form-control'
                          type='password'
                          name='password'
                          value={password}
                          onChange={this.handleChange}
                          placeholder='Password'
                        />
                        {submitted && user_id == {} && (
                          <div className='invalid-feedback'>
                            Password Required
                          </div>
                        )}
                      </div>
                      {this.showError()}
                      <div className='row'>
                        <div className='col-6'>
                          <button
                            className='btn btn-primary px-4 btn-login'
                            type='submit'
                            onClick={this.handleSubmit}
                          >
                            Login
                          </button>
                        </div>
                        <div className='col-6 text-right'>
                          <button className='btn btn-link px-0' type='button'>
                            Forgot password?
                          </button>
                          {loading && (
                            <img src='data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==' />
                          )}
                        </div>
                        <div className="col-6 mt-4">
                          <Link to="/register">Register your Business</Link>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
