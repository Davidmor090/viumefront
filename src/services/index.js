import get from './get';
import post from './post';
import update from './update';
import remove from './remove';

const api = {
  get,
  post,
  update,
  remove
}

export default api;