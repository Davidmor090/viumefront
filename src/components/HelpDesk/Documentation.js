import React, { Component } from 'react';
import Aside from '../Aside/Aside';
import Footer from '../Footer/Footer';
import Header from '../Header/Header';
import Sidebar from '../Sidebar/Sidebar';

export default class FAQs extends Component {
  render() {
    return (
      <div className="app header-fixed sidebar-expanded aside-menu-fixed sidebar-lg-show">
      <Header></Header>
        <div className="app-body">
          <Sidebar></Sidebar>
          <div className="container">
            <div className="row">
              <div className="col-8 mx-auto text-center my-2">
                <h1 className="my-4">Documentation</h1>
              </div>
              <div id="doc-index" className="card my-2 shadow col-12">
                <div className="px-2 mx-auto py-4 ml-5 mt-3">
                  <ol>
                    <li className="font-weight-bold text-uppercase">
                      Introduction
                      <ul className="font-weight-normal">
                        <li><a href="#">Overview</a></li>
                      </ul>
                    </li>
                    <li className="font-weight-bold text-uppercase">
                      SaaS
                      <ul className="font-weight-normal">
                        <li><a href="#">Overview</a></li>
                        <li><a href="#">Technology</a></li>
                        <li><a href="#">Dashboard (GUI)</a></li>
                        <li><a href="#">Recommendation Configuration</a></li>
                        <li><a href="#">Image Tagging</a></li>
                      </ul>
                    </li>
                    <li className="font-weight-bold text-uppercase">
                      Viume API
                      <ul className="font-weight-normal">
                        <li><a href="#">Description</a></li>
                        <li><a href="#">End points in our API</a></li>
                        <li>
                        <a href="#">Request Entry point</a>
                          <ul>
                            <li>Recommendation engine</li>
                            <li>Image tagging</li>
                          </ul>
                        </li>
                        <li>
                        <a href="">Request format</a>
                          <ul>
                            <li>Recommendation engine</li>
                            <li>Image tagging</li>
                          </ul>
                        </li>
                        <li><a href="">Example of request and response in our API</a></li>
                        <li><a href="">Authentication</a></li>
                      </ul>
                    </li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
          <Aside></Aside>
        </div>
        <Footer></Footer>
      </div>
    )
  }
}